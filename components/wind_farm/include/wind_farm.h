//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _WIND_FARM_H_
#define _WIND_FARM_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/touch_pad.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0      // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1      // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_19     // I2C Pins
#define PIN_SCL         GPIO_NUM_18

#define PIN_MOSI        GPIO_NUM_23     // SPI Pins
#define PIN_CLK         GPIO_NUM_21
#define PIN_CS			GPIO_NUM_22

#define PIN_WIND_1		GPIO_NUM_4      // Wind Indicator LEDs
#define PIN_WIND_2      GPIO_NUM_16 
#define PIN_WIND_3      GPIO_NUM_17 
#define PIN_WIND_4      GPIO_NUM_25 
#define PIN_WIND_5      GPIO_NUM_26
#define PIN_WIND_T      GPIO_NUM_2

#define IRQ_INA         GPIO_NUM_5      // INA233 Interrupt

// ---------- Touch Definitions ----------
#define PIN_TOUCH_S1    TOUCH_PAD_NUM4  // GPIO_NUM_13
#define PIN_TOUCH_S2    TOUCH_PAD_NUM5  // GPIO_NUM_12
#define PIN_TOUCH_S3    TOUCH_PAD_NUM6  // GPIO_NUM_14
#define PIN_TOUCH_S4    TOUCH_PAD_NUM7  // GPIO_NUM_27
#define PIN_TOUCH_S5    TOUCH_PAD_NUM8  // GPIO_NUM_33
#define PIN_TOUCH_S6    TOUCH_PAD_NUM9  // GPIO_NUM_32

#define TOUCH_THRESH_NO_USE   (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum pwm_channels{
    LED_WIND_SPEED_1 = 0,               // GPIO_NUM_26  Wind Speed LED
    LED_WIND_SPEED_2,                   // GPIO_NUM_25
    LED_WIND_SPEED_3,                   // GPIO_NUM_17
    LED_WIND_SPEED_4,                   // GPIO_NUM_16
    LED_WIND_SPEED_5,                   // GPIO_NUM_4
    PWM_WIND_TURBINE,                   // GPIO_NUM_2
    LED_WIND_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct WindFarm
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     setpoint_imax;      // Current output level
    float       wind_speed;         // Normalized wind speed
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_out;          // Output telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/wind_farm";
static const char mqtt_topic_sub[] = "/legos/wind_farm/cmd";


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t windfarm_init_entity();
esp_err_t windfarm_http_resp(char *str);
esp_err_t windfarm_http_read(char *str);
esp_err_t windfarm_mqtt_init(void *client);
esp_err_t windfarm_mqtt_post(void *client);
esp_err_t windfarm_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() windfarm_init_entity()
#define http_resp(x) windfarm_http_resp(x)
#define http_read(x) windfarm_http_read(x)
#define mqtt_init(x) windfarm_mqtt_init(x)
#define mqtt_post(x) windfarm_mqtt_post(x)
#define mqtt_read(x) windfarm_mqtt_read(x)

#endif //_WIND_FARM_H_