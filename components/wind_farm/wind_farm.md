# Solar Farm
The Solar Farm is a power producer from renewable energy, which converts the solar to electrical energy. It injects current in the grid using a current source configuration, able to erogate up to 1 A. In order to maximize the power conversion, a 1-axis solar tracker tilts the solar panel towards the direction of maximum intensity.

## Main code
### Status structure
```c
struct WindFarm
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     setpoint_imax;      // Current output level
    float       wind_speed;         // Normalized wind speed
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_out;          // Output telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> touch[Touch]
    touch --> telemetry[Telemetry]
    telemetry --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> touch[Detect Touch]
    touch --> ldo[Update Current]
    ldo --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/wind_farm**
    - Telemetry
        - *Format*: `WindFarm: <I> mA [<V> V]`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter.<br/>(*eg.* `WindFarm: 2121.7 mA [3.36 V]`)
- **/legos/service/cmd**
    - SCADA
        - Format: `wind|<val>`
        - Description: Log the total current output normalized to 1 A

### Subscribe
- **/legos/wind_farm/cmd**
    - IMAX
        - Format: `imax|<val>`
        - Description: Set the max bus current to **val** [0 to 255], equivalent approximately to [0 A to 1 A]
- **/legos/service/cmd**
    - Update
        - Format: `update|WindFarm`
        - Description: Update firmware

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>;load|<val>`
        - *Description*: Log telemetry data, **W** is the output power measured at the meter. Current load **val** is in the range [0.0 to 1.0](*eg.* `power|0.32;load|0.5`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Wind
        - Format: `wind|<val>`
        - Description: Set wind speed intensity to **val** [0.0 to 1.0]
### WEB PAGE
[<img src="docs/web_page.png"  width="121" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/wind_farm/wind_farm.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/wind_farm/wind_farm.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/wind_farm/wind_farm.md)