//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "wind_farm.h"
#include "esp_log.h"
#include "mqtt_client.h"

static const char *TAG = "WindFarm";                // Logging tag

static i2c_port_t i2c_num = I2C_NUM_1;              // I2C port
static uint8_t ina_grid = INA233_ADDRESS_44;        // I2C address INA233

spi_device_handle_t AD8400_IMAX;                    // SPI handle to Digital Pot
static uint8_t setpoint_imax_old;                   // Current setpoint

static uint16_t entity_timer_ms = 100;              // Main-tasks Thread interval
static uint64_t entity_timer_alarm;                 // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;             // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN,       // Timer configuration
                              TIMER_PAUSE,
                              TIMER_INTR_LEVEL,
                              TIMER_COUNT_UP,
                              TIMER_AUTORELOAD_EN,
                              TIMER_DIVIDER};

static esp_mqtt_client_handle_t mqtt_client;        // MQTT client handle
static esp_mqtt_event_handle_t mqtt_event;          // MQTT event handle

static uint16_t touchpad_thresh = 300;              // Touchpad sensitivity threshold
static uint16_t touchpad_offset[6];
static uint16_t touchpad_actual[6];
static touch_pad_t touch_slider[6] = {PIN_TOUCH_S1,
                                      PIN_TOUCH_S2,
                                      PIN_TOUCH_S3,
                                      PIN_TOUCH_S4,
                                      PIN_TOUCH_S5,
                                      PIN_TOUCH_S6};

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[LED_WIND_MAX] =
{
	{
        .channel = 0,
        .duty = 0,
        .gpio_num = PIN_WIND_1,        // LED_WIND_SPEED_1
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 1,
        .duty = 0,
        .gpio_num = PIN_WIND_2,        //LED_WIND_SPEED_2
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 2,
        .duty = 0,
        .gpio_num = PIN_WIND_3,        // LED_WIND_SPEED_3
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 3,
        .duty = 0,
        .gpio_num = PIN_WIND_4,        // LED_WIND_SPEED_4
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 4,
        .duty = 0,
        .gpio_num = PIN_WIND_5,        // LED_WIND_SPEED_5
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 5,
        .duty = 0,
        .gpio_num = PIN_WIND_T,        // LED_WIND_SPEED_5
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer =
    {
        .duty_resolution = LEDC_TIMER_13_BIT,   // PWM duty-cycle resolution
        .freq_hz = 5000,                        // PWM frequency
        .speed_mode = PWM_LS_MODE,              // PWM timer mode
        .timer_num = PWM_LS_TIMER,              // PWM timer index
        .clk_cfg = LEDC_AUTO_CLK,               // Auto select the source clock
};


// SPI bus configuration
static spi_bus_config_t buscfg =
    {
        .miso_io_num = -1,                      // -1 if not used
        .mosi_io_num = PIN_MOSI,                // SPI MOSI
        .sclk_io_num = PIN_CLK,                 // SPI CLK
        .quadwp_io_num = -1,                    // -1 if not used
        .quadhd_io_num = -1                     // -1 if not used
};

// SPI device configuration
spi_device_interface_config_t AD8400_IMAX_cfg =
    {
        .clock_speed_hz = 1000000,              // SPI Clock out at 1 MHz
        .mode = 0,                              // SPI Mode 0
        .spics_io_num = PIN_CS,                 // SPI CS
        .queue_size = 1,                        // SPI queue dimension
        //.pre_cb=spi_pre_transfer_callback,    // Specify pre-transfer callback to handle D/C line
};

//
//------------------------------------------------------------SERVICE FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 * 
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty, fade_time_ms);
    ledc_fade_start(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG,"PWM fade\tch:%d@%d in %d ms",ch,duty,fade_time_ms);
}


/**
 * @brief Set PWM channel dutycycle
 * 
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * 
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG,"PWM switch\tch:%d@%d",ch,duty);
}


/**
 * @brief Set Digital Potentiometer
 * 
 * @param[in] handle SPI device handle
 * @param[in] value Value
 * 
 * @return void
 */
void set_digital_pot(spi_device_handle_t handle, uint8_t value)
{
    uint8_t buffer[2];

    // Serial Data-Word Format [A1 A0 D7 D6 D5 D4 D3 D2, D1 D0]
    buffer [0] = value >> 2;                                    // MSB zero padding 
    buffer [1] = value << 6;

    spi_transaction_t t;
    memset(&t, 0, sizeof(t));                                   // Zero out the transaction
    t.length = 10;                                              // Command is 10 bits
    t.tx_buffer = &buffer;                                      // The data is the cmd itself
    // t.tx_buffer=&cmd;                                        // The data is the cmd itself
    ESP_ERROR_CHECK(spi_device_transmit(handle, &t));
}

/**
 * @brief Read Touch-slider level
 * 
 * @param[in,out] touch_value Touch-slider measurement centroid
 * 
 * @return bool
 */
bool read_slider(uint8_t *touch_value)
{
    // Update touch readings
    for (int i=0; i<6; i++)
        touch_pad_read_filtered(touch_slider[i], &touchpad_actual[i]);
    
    uint32_t sum_w = 0;
    uint32_t sum = 0;
    uint32_t dtouch = 0;

    for (int i=0; i<6; i++){
        dtouch = abs(touchpad_offset[i]-touchpad_actual[i]);
        sum_w += dtouch*i*51;
        sum += dtouch;
    }
    *touch_value = (sum_w/sum);

    return (sum > touchpad_thresh);
}


/**
 * @brief Read INA233 Telemetry
 * 
 * @param[in] addr Addres of the device on the I2C bus
 * 
 * @return void
 */
void read_telemetry(uint8_t addr)
{
    entity.power_out.voltage = ina233_getBusVoltage_V(addr);
    entity.power_out.current = ina233_getCurrent_mA(addr);
    entity.power_out.power = ina233_getPower_mW(addr);
    ESP_LOGD(TAG,"Telemetry\tbus: %f V,shunt: %f mA, pwr: %f mW",entity.power_out.voltage,entity.power_out.current,entity.power_out.power);   
}


/**
 * @brief Entity main task timer ISR
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
	// Clear interrupt
	TIMERG0.int_clr_timers.t0 = 1;

	// Restart timer
	timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

	// Notify task
	xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
	portYIELD_FROM_ISR();
}


/**
 * @brief Update wind speed LED indicator
 * 
 * @param[in] led_intensity Normalized LED intensity
 * 
 * @return void
 */
void update_led_indicator(float led_intensity)
{
    uint8_t step_fixed = entity.wind_speed*(LED_WIND_MAX-1);
    float   step_fade  = entity.wind_speed*(LED_WIND_MAX-1) - step_fixed;
    step_fade *= step_fade;

    // Set LED status
    for (int i = 0; i < step_fixed; i++)
        pwm_switch(i, led_intensity);
    pwm_switch(step_fixed, step_fade*led_intensity);
    for (int i = step_fixed+1; i < (LED_WIND_MAX-1); i++)
        pwm_switch(i, LED_INTENSITY_0);

}


/**
 * @brief Entity main task
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void entity_main_tasks(void* arg)
{
    setpoint_imax_old = 0;
    uint8_t slider_pos = 0;

	while (1)
	{
		// Wait timer
		xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

		// Get data
		read_telemetry(ina_grid);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity*LED_INTENSITY_100;

        // Get touchpad status
        if (read_slider(&slider_pos))
            entity.setpoint_imax = slider_pos;

        // No change
        if  (entity.setpoint_imax == setpoint_imax_old)
            continue;

        // Update Wind Speed LED indicator
        entity.wind_speed = ((float)slider_pos/255.0);
        update_led_indicator(led_intensity);
        pwm_switch(PWM_WIND_TURBINE, LED_INTENSITY_100);
        pwm_fade(PWM_WIND_TURBINE, (3 + 2*entity.wind_speed)/5*8198, 1000);

        // Update current limit
        setpoint_imax_old = entity.setpoint_imax;
        set_digital_pot(AD8400_IMAX, entity.setpoint_imax);
	}
	vTaskDelete(NULL);
}

//
//------------------------------------------------------------INIT FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Init GPIO
 * 
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{

    // INA IRQ Missing
    return ESP_OK;
}


/**
 * @brief Init PWM channels
 * 
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer0 for low speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_WIND_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0));                                     // Initialize fade service
}


/**
 * @brief Init 1-Wire interface
 * 
 *        Initialize the 1-Wire interface and detect the 64-bit ID eeprom connected to
 *        the bus A and B
 * @note  Return ESP_FAIL in case of branch not terminated on a node and trigger a reset
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t init_owb()
{
    // esp_log_level_set("owb", ESP_LOG_INFO);
    // esp_log_level_set("owb_rmt", ESP_LOG_INFO);

    // Create a 1-Wire bus, using the RMT timeslot driver
    OneWireBus *owb;
    owb_rmt_driver_info rmt_driver_info;
    owb = owb_rmt_initialize(&rmt_driver_info, PIN_EEPROM, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true); // enable CRC check for ROM code
    owb_use_parasitic_power(owb, true);

    // Find connected device
    OneWireBus_SearchState search_state = {0};
    bool found = false;
    owb_search_first(owb, &search_state, &found);
    OneWireBus_ROMCode device_rom_code = search_state.rom_code;
    
    ESP_LOGD(TAG, "Node ID:");
    if (found)
    {
        esp_log_buffer_hex_internal(TAG, device_rom_code.bytes, 8, ESP_LOG_DEBUG);
        memcpy(entity.id, device_rom_code.bytes, 8);
        return ESP_OK;
    }
    else
    {
        ESP_LOGD(TAG, "not valid");
        return ESP_FAIL;
    }
}


/**
 * @brief Init the I2C interface
 * 
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
esp_err_t init_i2c()
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    ESP_LOGD(TAG, "sda_io_num %d", PIN_SDA);

    conf.sda_io_num = PIN_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "scl_io_num %d", PIN_SCL);

    conf.scl_io_num = PIN_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "clk_speed %d", I2C_MASTER_FREQ_HZ);

    conf.clk_flags = 0;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    ESP_LOGD(TAG, "i2c_param_config %d", conf.mode);
    ESP_ERROR_CHECK(i2c_param_config(i2c_num, &conf));

    ESP_LOGD(TAG, "i2c_driver_install %d", i2c_num);
    return (i2c_driver_install(i2c_num, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0));
}


/**
 * @brief Init the SPI Interface
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_spi()
{
    ESP_ERROR_CHECK(spi_bus_initialize(HSPI_HOST, &buscfg, 0));             //Initialize the SPI bus
    ESP_ERROR_CHECK(spi_bus_add_device(HSPI_HOST, &AD8400_IMAX_cfg, &AD8400_IMAX));

    return ESP_OK;
}


/**
 * @brief Init Touch Channels
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_touch()
{
    ESP_ERROR_CHECK(touch_pad_init());   // The default fsm mode is software trigger mode.

    ESP_ERROR_CHECK(touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V)); // set ref voltage vor charging/discharging
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S1, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S2, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S3, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S4, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S5, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_S6, TOUCH_THRESH_NO_USE));
    ESP_ERROR_CHECK(touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD));

    // Update non-touch status
    for (int i=0; i<6; i++)
        touch_pad_read_filtered(touch_slider[i], &touchpad_offset[i]);

    return ESP_OK;
}


/**
 * @brief Init the INA233 power monitor
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_telemetry()
{
    /*
    +5V Rail
    | Part      | Count     | Typical | Max     |
    | --------- | :-------: | ------: | ------: |
    | LT3081    |  1        |         | 1200 mA |
    | Total     |           |         | 1200 mA |
    */

    float INA_R_SHUNT = 0.075;
    float INA_I_MAX = 1.2;

    ESP_ERROR_CHECK(ina233_init(i2c_num, ina_grid));
    ESP_ERROR_CHECK(ina233_setCalibration(ina_grid, INA_R_SHUNT, INA_I_MAX));
    return ESP_OK;
}


/**
 * @brief Init timer for main entity task
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
	entity_timer_alarm = (uint64_t) entity_timer_ms * TIMER_SCALE / 1000ULL;

	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
	ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_isr_register(TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));
    
	ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

	return ESP_OK;
}


/**
 * @brief Init entity
 *        Initialize the entity-specific submodules and start the activity tasks
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t windfarm_init_entity()
{
    // Set default LED intensity
    entity.led_intensity = 0.5;

    // Set default setpoints
    entity.setpoint_imax = 127;

    init_gpio();
    init_pwm();
    init_owb();
    init_i2c();
    init_spi();
    init_touch();
    init_telemetry();

    // Entity main tasks
	xTaskCreate(entity_main_tasks, "entity_main_task", 2048, NULL, 4, &entity_task_handle);
	ESP_ERROR_CHECK(init_tasks_timer());

    return ESP_OK;
}

//
//------------------------------------------------------------EXTERNAL FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t windfarm_http_resp(char* str)
{
    sprintf(str, "power|%.2f;load|%.2f",
            entity.power_out.power/1000,
            entity.power_out.current/1000.0);

	return ESP_OK;
}



/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t windfarm_http_read(char *str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
    }

    if (msg_unmarshall(str, "wind", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.setpoint_imax = val*255;
                ESP_LOGD(TAG, "WIND");
            }
            return ESP_OK;
    }

    return ESP_FAIL;
}


/**
 * @brief MQTT init topics
 *        Subscribe to entity-specific topics
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t windfarm_mqtt_init(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_service, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_service);

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_sub, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_sub);

    return ESP_OK;
}


/**
 * @brief MQTT post
 *        Callback for periodic publishing updates via MQTT
 * 
 * @param[in] client Pointer to MQTT client handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t windfarm_mqtt_post(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;
    char data[64];
    sprintf(data,"%10s: %7.1f mA [%4.2f V]",TAG,entity.power_out.current,entity.power_out.voltage);
    esp_mqtt_client_publish(mqtt_client, mqtt_topic_pub, data, 0, 0, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);

    memset(data,0,64);
    sprintf(data,"wind|%.2f",entity.power_out.current/1000.0);
    esp_mqtt_client_publish(mqtt_client, mqtt_topic_service, data, 0, 0, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_service, data);

    return ESP_OK;
}


/**
 * @brief MQTT read
 *        Callback for reading subscribed topics via MQTT
 * 
 * @param[in] event Pointer to MQTT event handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t windfarm_mqtt_read(void *event)
{
    mqtt_event = (esp_mqtt_event_handle_t)event;

    char* data = (char*)malloc(mqtt_event->data_len+1);
    memcpy(data,mqtt_event->data,mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len+1);
    memcpy(topic,mqtt_event->topic,mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic,mqtt_topic_service)){
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val,TAG)){
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return ESP_OK;
            }

        if (msg_unmarshall(data, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
        }
    }

    if (!strcmp(topic,mqtt_topic_sub)){
        if (msg_unmarshall(data, "imax", &msg_payload)){
            uint8_t val = atoi(msg_payload.val);
            entity.setpoint_imax = val;
            ESP_LOGD(TAG, "BUS CURRENT");
            return ESP_OK;
        }
    }
    return ESP_OK;
}