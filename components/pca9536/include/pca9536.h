//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _PCA9536_H_
#define _PCA9536_H_

#include "driver/i2c.h"

// Datasheet    https://www.ti.com/lit/ds/symlink/pca9536.pdf

// I2C Address
#define PCA9536_ADDRESS (0x41) // 1000001


// Device Registers
//      Name            Code    Function                            R/W     Size    Default
#define PCA_REG_IN      (0x00)  // Input Port register              R       1       N/A
#define PCA_REG_OUT     (0x01)  // Output Port register             R/W     1       0xFF
#define PCA_REG_POL     (0x02)  // Polarity Inversion register      R/W     2       0x00
#define PCA_REG_CONF    (0x03)  // Configuration register           R/W     2       0xFF


// I2C definitions
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define ACK_CHECK_EN 0x1            // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0           // I2C master will not check ack from slave 
#define ACK_VAL 0x0                 // I2C ack value 
#define NACK_VAL 0x1                // I2C nack value 
#define I2C_MASTER_TX_BUF_DISABLE 0 // I2C master do not need buffer 
#define I2C_MASTER_RX_BUF_DISABLE 0 // I2C master do not need buffer 
#define I2C_MASTER_FREQ_HZ 400000   // I2C master clock frequency 


#ifdef __cplusplus
extern "C" {
#endif

    void pca9536_set_gpio_level(uint8_t ch, uint8_t level);
    void pca9536_get_gpio_level(uint8_t ch, uint8_t *level);
    esp_err_t pca9536_init(i2c_port_t v_i2c_num, uint8_t addr);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_PCA9536_H_