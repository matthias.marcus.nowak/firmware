//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _SOLAR_FARM_H_
#define _SOLAR_FARM_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/pcnt.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/adc.h"
#include "driver/i2c.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0      // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1      // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_15     // I2C Pins
#define PIN_SCL         GPIO_NUM_13

#define PIN_MOSI        GPIO_NUM_27     // SPI Pins
#define PIN_CLK         GPIO_NUM_14
#define PIN_CS			GPIO_NUM_26     // Poti for Currentlimit

#define PIN_PV_1        GPIO_NUM_4      // Solar Indicator LEDs
#define PIN_PV_2        GPIO_NUM_16 
#define PIN_PV_3        GPIO_NUM_17 
#define PIN_PV_4        GPIO_NUM_18 
#define PIN_PV_5		GPIO_NUM_19
#define PIN_PV_T		GPIO_NUM_21

#define PIN_SENS_R      GPIO_NUM_22     // Light direction measurment
#define PIN_SENS_L      GPIO_NUM_23
#define PIN_SENS_LR     GPIO_NUM_32

#define PIN_PV          ADC1_CHANNEL_5  // GPIO_NUM_33 - PV Light intensity

#define IRQ_INA         GPIO_NUM_5      // INA233 Interrupt

//PWM Channels
#define Servo           (0)             // GPIO_NUM_21 Solar Tracker

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum pwm_channels{
    LED_PV_PWR_1 = 0,               // GPIO_NUM_4  PV power LED
    LED_PV_PWR_2,                   // GPIO_NUM_16
    LED_PV_PWR_3,                   // GPIO_NUM_17
    LED_PV_PWR_4,                   // GPIO_NUM_18
    LED_PV_PWR_5,                   // GPIO_NUM_19
    PWM_PV_TRACKING,                // GPIO_NUM_21
    LED_PV_MAX,
};

#define PWM_SOLAR_TRACK LED_WIND_MAX

enum servo_pos{
    SERVO_POS_0   = 450,            // Servo position   0°
    SERVO_POS_90  = 614,            // Servo position  90°
    SERVO_POS_180 = 828,            // Servo position 180°
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct SolarFarm
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     setpoint_imax;      // Current output level
    float       pv_intensity;       // Normalized light intensity
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_out;          // Output telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/solar_farm";
static const char mqtt_topic_sub[] = "/legos/solar_farm/cmd";


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t solarfarm_init_entity();
esp_err_t solarfarm_http_resp(char *str);
esp_err_t solarfarm_http_read(char *str);
esp_err_t solarfarm_mqtt_init(void *client);
esp_err_t solarfarm_mqtt_post(void *client);
esp_err_t solarfarm_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() solarfarm_init_entity()
#define http_resp(x) solarfarm_http_resp(x)
#define http_read(x) solarfarm_http_read(x)
#define mqtt_init(x) solarfarm_mqtt_init(x)
#define mqtt_post(x) solarfarm_mqtt_post(x)
#define mqtt_read(x) solarfarm_mqtt_read(x)

#endif //_SOLAR_FARM_H_