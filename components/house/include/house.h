//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _HOUSE_H_
#define _HOUSE_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"
#include "driver/adc.h"
#include "driver/dac.h"
#include "driver/touch_pad.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "tmp100.h"
#include "pca9536.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------

#define PIN_EEPROM      GPIO_NUM_0      // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1      // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_5      // I2C Pins
#define PIN_SCL         GPIO_NUM_18

#define PIN_MOSI        GPIO_NUM_22     // SPI Pins
#define PIN_CLK         GPIO_NUM_21 
#define PIN_CS			GPIO_NUM_23     // Poti for Currentlimit

#define PIN_BRIGHT      GPIO_NUM_12     // 7 SEG brightness
#define PIN_SEG_0       GPIO_NUM_13     // 7 SEG enable
#define PIN_SEG_1       GPIO_NUM_16
#define PIN_SEG_2       GPIO_NUM_17

#define PIN_BCD_0       GPIO_NUM_25     // 7 SEG value in BCD
#define PIN_BCD_1       GPIO_NUM_14
#define PIN_BCD_2       GPIO_NUM_27
#define PIN_BCD_3       GPIO_NUM_26

#define PIN_SRC         GPIO_NUM_19     // Power source selection (0 GRID, 1 SOLAR)
#define PIN_TEM_HEAT    GPIO_NUM_32     // TEM heating
#define PIN_TEM_COOL    GPIO_NUM_33     // TEM cooling
#define PIN_PV          ADC1_CHANNEL_6  // GPIO_NUM_34 Light intensity of Solarcell

#define IRQ_INA_BUS     GPIO_NUM_39     // INA233 Bus Interrupt
#define IRQ_INA_EXT     GPIO_NUM_36     // INA233 Ext Interrupt

#define GPIOX_STOVE     (0)
#define GPIOX_OVEN      (1)
#define GPIOX_AC        (2)
#define GPIOX_HEAT      (3)

// ---------- Touch Definitions ----------
#define PIN_TOUCH_NEG   TOUCH_PAD_NUM2  // GPIO_NUM_2
#define PIN_TOUCH_POS   TOUCH_PAD_NUM3  // GPIO_NUM_15 

#define TOUCH_THRESH_NO_USE   (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

// ---------- PWM channels Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum pwm_channels{
    PWM_SEG_BRIGHT = 0,                 // GPIO_NUM_12
    PWM_TEM_HEAT,                       // GPIO_NUM_32
    PWM_TEM_COOL,                       // GPIO_NUM_33
    PWM_SOURCE,                         // GPIO_NUM_19
    PWM_MAX
};

// ---------- Entity Definitions ----------

/**
 * @brief Temperature measurements structure
 */
typedef struct
{
    float inside;
    float outside;
    float target;
} temp_t;

/**
 * @brief Entity status structure
 */
struct House
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     setpoint_imax;      // Current output level
    temp_t      temp;               // Temperature monitor
    float       pv_intensity;       // Normalized light intensity
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
    telemetry_t power_out;          // Output telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/house";
static const char mqtt_topic_sub[] = "/legos/house/cmd";

#ifdef __cplusplus
extern "C" {
#endif

esp_err_t house_init_entity();
esp_err_t house_http_resp(char *str);
esp_err_t house_http_read(char *str);
esp_err_t house_mqtt_init(void *client);
esp_err_t house_mqtt_post(void *client);
esp_err_t house_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() house_init_entity()
#define http_resp(x) house_http_resp(x)
#define http_read(x) house_http_read(x)
#define mqtt_init(x) house_mqtt_init(x)
#define mqtt_post(x) house_mqtt_post(x)
#define mqtt_read(x) house_mqtt_read(x)

#endif //_HOUSE_H_