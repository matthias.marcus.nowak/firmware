//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "house.h"
#include "esp_log.h"
#include "mqtt_client.h"

static const char *TAG = "House";                   // Logging tag

static i2c_port_t i2c_num = I2C_NUM_1;              // I2C port
static const uint8_t ina_grid = INA233_ADDRESS_40;  // I2C address INA233 Bus
static const uint8_t ina_pv   = INA233_ADDRESS_44;  // I2C address INA233 Ext
static uint8_t tmp_out  = TMP100_ADDRESS_72;        // I2C address TMP100 Outside
static uint8_t tmp_in   = TMP100_ADDRESS_76;        // I2C address TMP100 Inside
static uint8_t gpio_x   = PCA9536_ADDRESS;          // I2C address PCA9536

spi_device_handle_t AD8400_IMAX;                    // SPI handle to Digital Pot
static uint8_t setpoint_imax_old;                   // Current setpoint

static uint16_t display_timer_ms = 1;               // Display-tasks Thread interval
static uint64_t display_timer_alarm;                // Display-tasks Thread timer
static TaskHandle_t display_task_handle;            // Display-tasks Thread task handle
static uint16_t entity_timer_ms = 500;              // Main-tasks Thread interval
static uint64_t entity_timer_alarm;                 // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;             // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN,       // Timer configuration
                              TIMER_PAUSE,
                              TIMER_INTR_LEVEL,
                              TIMER_COUNT_UP,
                              TIMER_AUTORELOAD_EN,
                              TIMER_DIVIDER};

static esp_mqtt_client_handle_t mqtt_client;        // MQTT client handle
static esp_mqtt_event_handle_t mqtt_event;          // MQTT event handle
static uint8_t remote_override = 0;
static float   remote_target = 0;
static uint16_t touchpad_thresh = 600;              // Touchpad sensitivity threshold

static uint8_t PIN_DIG[3] = {PIN_SEG_0, PIN_SEG_1, PIN_SEG_2};
static uint8_t PIN_BCD[4] = {PIN_BCD_0, PIN_BCD_1, PIN_BCD_2, PIN_BCD_3};
static uint8_t DIG_BUF[3];
static uint8_t BCD_BUF[4][3];

// LEDC channels configuration
ledc_channel_config_t pwm_channel[PWM_MAX] =
{
    {
        .channel = 0, 
        .duty = 0,
        .gpio_num = PIN_BRIGHT,         // SEG_bright
        .speed_mode = PWM_LS_MODE,      
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER       
    },
    {
        .channel = 1, 
        .duty = 0,
        .gpio_num = PIN_TEM_HEAT,       // TEM_heat
        .speed_mode = PWM_LS_MODE,      
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER       
    },
    {
        .channel = 2, 
        .duty = 0,
        .gpio_num = PIN_TEM_COOL,       // TEM_cool
        .speed_mode = PWM_LS_MODE,      
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER       
    },
    {
        .channel = 3, 
        .duty = 0,
        .gpio_num = PIN_SRC,            // Source
        .speed_mode = PWM_LS_MODE,      
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER       
    },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer =
    {
        .duty_resolution = LEDC_TIMER_13_BIT,   // PWM duty-cycle resolution
        .freq_hz = 5000,                        // PWM frequency
        .speed_mode = PWM_LS_MODE,              // PWM timer mode
        .timer_num = PWM_LS_TIMER,              // PWM timer index
        .clk_cfg = LEDC_AUTO_CLK,               // Auto select the source clock
};

// SPI bus configuration
static spi_bus_config_t buscfg =
    {
        .miso_io_num = -1,                      // -1 if not used
        .mosi_io_num = PIN_MOSI,                // SPI MOSI
        .sclk_io_num = PIN_CLK,                 // SPI CLK
        .quadwp_io_num = -1,                    // -1 if not used
        .quadhd_io_num = -1                     // -1 if not used
};

// SPI device configuration
spi_device_interface_config_t AD8400_IMAX_cfg =
    {
        .clock_speed_hz = 1000000,              // SPI Clock out at 1 MHz
        .mode = 0,                              // SPI Mode 0
        .spics_io_num = PIN_CS,                 // SPI CS
        .queue_size = 1,                        // SPI queue dimension
        //.pre_cb=spi_pre_transfer_callback,    // Specify pre-transfer callback to handle D/C line
};

//
//------------------------------------------------------------SERVICE FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 * 
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty, fade_time_ms);
    ledc_fade_start(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG,"PWM fade\tch:%d@%d in %d ms",ch,duty,fade_time_ms);
}


/**
 * @brief Set PWM channel dutycycle
 * 
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * 
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG,"PWM switch\tch:%d@%d",ch,duty);
}


/**
 * @brief Set Digital Potentiometer
 * 
 * @param[in] handle SPI device handle
 * @param[in] value Value
 * 
 * @return void
 */
void set_digital_pot(spi_device_handle_t handle, uint8_t value)
{
    uint8_t buffer[2];

    // Serial Data-Word Format [A1 A0 D7 D6 D5 D4 D3 D2, D1 D0]
    buffer [0] = value >> 2;                                    // MSB zero padding 
    buffer [1] = value << 6;

    spi_transaction_t t;
    memset(&t, 0, sizeof(t));                                   // Zero out the transaction
    t.length = 10;                                              // Command is 10 bits
    t.tx_buffer = &buffer;                                      // The data is the cmd itself
    // t.tx_buffer=&cmd;                                        // The data is the cmd itself
    ESP_ERROR_CHECK(spi_device_transmit(handle, &t));
}


/**
 * @brief Refresh 7SEG Display
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void display_refresh_task(void *arg)
{   
    uint8_t d, b;

    while(1) {
        for (d=0; d<3; d++){
            gpio_set_level(PIN_DIG[0],0);
            gpio_set_level(PIN_DIG[1],0);
            gpio_set_level(PIN_DIG[2],0);
            for (b=0; b<4; b++)
                gpio_set_level(PIN_BCD[b],BCD_BUF[b][d]);
            gpio_set_level(PIN_DIG[d],1);

            xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);
        }
    }
}


/**
 * @brief Set 7SEG Display Value
 * 
 * @param[in] val_f Value to set
 * 
 * @return bool
 */
void set_display_value(float val_f)
{   
    uint16_t value = 10.0*val_f;
     
    DIG_BUF[0] = value % 10; value /= 10;
    DIG_BUF[1] = value % 10; value /= 10;
    DIG_BUF[2] = value % 10;

    for (int d=0; d<3; d++)
        for (int b=0; b<4; b++)
            BCD_BUF[b][d] = ((DIG_BUF[d] >> b) & 0x01);
}


/**
 * @brief Read Touchpad level
 * 
 * @param[in] ch Touchpad channel
 * 
 * @return bool
 */
bool read_touch(touch_pad_t ch)
{
    uint16_t touch_value;
    touch_pad_read_filtered(ch, &touch_value);

    return (touch_value < touchpad_thresh);
}


/**
 * @brief Read INA233 Telemetry
 * 
 * @param[in] addr Addres of the device on the I2C bus
 * 
 * @return void
 */
void read_telemetry(uint8_t addr)
{
    switch (addr){
        case ina_grid:
        {
            entity.power_in.voltage = ina233_getBusVoltage_V(addr);
            entity.power_in.current = ina233_getCurrent_mA(addr);
            entity.power_in.power = ina233_getPower_mW(addr);
            ESP_LOGD(TAG,"Telemetry\tbus: %f V,shunt: %f mA, pwr: %f mW",entity.power_in.voltage,entity.power_in.current,entity.power_in.power);
            break;
        }
        case ina_pv:
        {
            entity.power_out.voltage = ina233_getBusVoltage_V(addr);
            entity.power_out.current = ina233_getCurrent_mA(addr);
            entity.power_out.power = ina233_getPower_mW(addr);
            ESP_LOGD(TAG,"Telemetry PV\tbus: %f V,shunt: %f mA, pwr: %f mW",entity.power_out.voltage,entity.power_out.current,entity.power_out.power);
            break;
        }
    }    
}


/**
 * @brief Entity main task timer ISR
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
	// Clear interrupt
	TIMERG0.int_clr_timers.t0 = 1;

	// Restart timer
	timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

	// Notify task
	xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
	portYIELD_FROM_ISR();
}


/**
 * @brief Display task timer ISR
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void IRAM_ATTR display_timer_isr(void* arg)
{
	// Clear interrupt
	TIMERG0.int_clr_timers.t1 = 1;

	// Restart timer
	timer_set_alarm(TIMER_GROUP_0, TIMER_1, TIMER_ALARM_EN);

	// Notify task
	xTaskNotifyFromISR(display_task_handle, 0x00, eNoAction, NULL);
	portYIELD_FROM_ISR();
}


/**
 * @brief Entity main task
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void entity_main_tasks(void* arg)
{
    int8_t target_cnt = -1;
    uint32_t appliance_cnt = 0;
    setpoint_imax_old = 0;

	while (1)
	{
		// Wait timer
		xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

		// Get data
		read_telemetry(ina_grid);
        read_telemetry(ina_pv);       

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity*LED_INTENSITY_100;

        // Set display intensity
        pwm_switch(PWM_SEG_BRIGHT, led_intensity);

        // Get PV intensity
        entity.pv_intensity = ((float)adc1_get_raw(PIN_PV))/4095.0;    // (ADC / 2^12)
        entity.setpoint_imax = entity.pv_intensity*255.0;

        // Set energy source mix
        float source = entity.pv_intensity;
        pwm_fade(PWM_SOURCE, source*LED_INTENSITY_100, 100);

        // Get touchpad status
        if (read_touch(PIN_TOUCH_POS)){
            target_cnt = 0;
            entity.temp.target += 0.1;          // 0.1°C increment per touch
            
        }
        if (read_touch(PIN_TOUCH_NEG)){
            target_cnt = 0;
            entity.temp.target -= 0.1;          // 0.1°C decrement per touch
            
        }
        
        // Get temperature
        entity.temp.inside  = tmp100_readTemp(tmp_in);
        entity.temp.outside = tmp100_readTemp(tmp_out);
                
        if (entity.temp.target > (entity.temp.outside + 5.0))
            entity.temp.target = (entity.temp.outside + 5.0);
        if (entity.temp.target < (entity.temp.outside - 5.0))
            entity.temp.target = (entity.temp.outside - 5.0);

        // Display temperature
        if (target_cnt > -1) {
            set_display_value(entity.temp.target);
            target_cnt = (target_cnt < 3) ? target_cnt + 1 : -1;
        } else
            set_display_value(entity.temp.inside);

        // Temperature proportional control action with hysteresis
        float target;
        if (!remote_override){
            target = entity.temp.target;
        } else {
            target = entity.temp.inside + remote_target;
            remote_override--;
        }

        uint16_t dc = (fabs(target-entity.temp.inside)/5.0)*8191.0;
        dc = (dc > 8191) ? 8191 : dc;
        if (entity.temp.inside < (target - 0.3)){
            pwm_switch(PWM_TEM_COOL, 0);
            pwm_switch(PWM_TEM_HEAT, dc);
            pca9536_set_gpio_level(GPIOX_AC,0);
            pca9536_set_gpio_level(GPIOX_HEAT,1);
        } else if (entity.temp.inside > (target + 0.3)){
            pwm_switch(PWM_TEM_HEAT, 0);
            pwm_switch(PWM_TEM_COOL, dc);
            pca9536_set_gpio_level(GPIOX_AC,1);
            pca9536_set_gpio_level(GPIOX_HEAT,0);
        } else {
            pwm_switch(PWM_TEM_COOL, 0);
            pwm_switch(PWM_TEM_HEAT, 0);
            pca9536_set_gpio_level(GPIOX_AC,0);
            pca9536_set_gpio_level(GPIOX_HEAT,0);
        }

        // Appliance control
        if ((appliance_cnt/30) % 2){
            pca9536_set_gpio_level(GPIOX_STOVE,1);
            pca9536_set_gpio_level(GPIOX_OVEN, 1);
        } else {
            pca9536_set_gpio_level(GPIOX_STOVE,0);
            pca9536_set_gpio_level(GPIOX_OVEN, 0);
        }
        appliance_cnt++;

        // Update current limit
        if  (abs(entity.setpoint_imax - setpoint_imax_old) > 2){
            setpoint_imax_old = entity.setpoint_imax;
            set_digital_pot(AD8400_IMAX, entity.setpoint_imax);
        }
	}
	vTaskDelete(NULL);
}

//
//------------------------------------------------------------INIT FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Init GPIO
 * 
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = ((1ULL << PIN_BCD_0) |
                            (1ULL << PIN_BCD_1) |
                            (1ULL << PIN_BCD_2) |
                            (1ULL << PIN_BCD_3) |
                            (1ULL << PIN_SEG_0) |
                            (1ULL << PIN_SEG_1) |
                            (1ULL << PIN_SEG_2));
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    gpio_set_level(PIN_BCD_0, 0);
    gpio_set_level(PIN_BCD_1, 0);
    gpio_set_level(PIN_BCD_2, 0);
    gpio_set_level(PIN_BCD_3, 0);
    gpio_set_level(PIN_SEG_0, 0);
    gpio_set_level(PIN_SEG_1, 0);
    gpio_set_level(PIN_SEG_2, 0);

    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(PIN_PV, ADC_ATTEN_DB_0);

    // INA IRQ Missing
    return ESP_OK;
}


/**
 * @brief Init PWM channels
 * 
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer0 for low speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < PWM_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0));                                     // Initialize fade service
}


/**
 * @brief Init 1-Wire interface
 * 
 *        Initialize the 1-Wire interface and detect the 64-bit ID eeprom connected to
 *        the bus A and B
 * @note  Return ESP_FAIL in case of branch not terminated on a node and trigger a reset
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t init_owb()
{
    // esp_log_level_set("owb", ESP_LOG_INFO);
    // esp_log_level_set("owb_rmt", ESP_LOG_INFO);

    // Create a 1-Wire bus, using the RMT timeslot driver
    OneWireBus *owb;
    owb_rmt_driver_info rmt_driver_info;
    owb = owb_rmt_initialize(&rmt_driver_info, PIN_EEPROM, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true); // enable CRC check for ROM code
    owb_use_parasitic_power(owb, true);

    // Find connected device
    OneWireBus_SearchState search_state = {0};
    bool found = false;
    owb_search_first(owb, &search_state, &found);
    OneWireBus_ROMCode device_rom_code = search_state.rom_code;
    
    ESP_LOGD(TAG, "Node ID:");
    if (found)
    {
        esp_log_buffer_hex_internal(TAG, device_rom_code.bytes, 8, ESP_LOG_DEBUG);
        memcpy(entity.id, device_rom_code.bytes, 8);
        return ESP_OK;
    }
    else
    {
        ESP_LOGD(TAG, "not valid");
        return ESP_FAIL;
    }
}


/**
 * @brief Init the I2C interface
 * 
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
esp_err_t init_i2c()
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    ESP_LOGD(TAG, "sda_io_num %d", PIN_SDA);

    conf.sda_io_num = PIN_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "scl_io_num %d", PIN_SCL);

    conf.scl_io_num = PIN_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "clk_speed %d", I2C_MASTER_FREQ_HZ);

    conf.clk_flags = 0;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    ESP_LOGD(TAG, "i2c_param_config %d", conf.mode);
    ESP_ERROR_CHECK(i2c_param_config(i2c_num, &conf));

    ESP_LOGD(TAG, "i2c_driver_install %d", i2c_num);
    return (i2c_driver_install(i2c_num, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0));
}


/**
 * @brief Init the SPI Interface
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_spi()
{
    ESP_ERROR_CHECK(spi_bus_initialize(HSPI_HOST, &buscfg, 0));             //Initialize the SPI bus       
    return(spi_bus_add_device(HSPI_HOST, &AD8400_IMAX_cfg, &AD8400_IMAX));                 //Attach the Poti to the SPI bus
}


/**
 * @brief Init Touch Channels
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_touch()
{
    ESP_ERROR_CHECK(touch_pad_init());   // The default fsm mode is software trigger mode.

    ESP_ERROR_CHECK(touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V)); // set ref voltage vor charging/discharging
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_POS, TOUCH_THRESH_NO_USE));                          // insert touchpad channels that are used (see schematic) 
    ESP_ERROR_CHECK(touch_pad_config(PIN_TOUCH_NEG, TOUCH_THRESH_NO_USE));                          // channel no. are defined in the GPIO Tabel with names 
    ESP_ERROR_CHECK(touch_pad_filter_start(TOUCHPAD_FILTER_TOUCH_PERIOD));
   
    return ESP_OK;
}


/**
 * @brief Init the INA233 power monitor
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_telemetry()
{
    /*
    VBus Rail
    | Part      | Count     | Typical | Max     |
    | --------- | :-------: | ------: | ------: |
    | ESP32     | 1         | 160 mA  |  260 mA |
    | TEM       | 1         |         |  500 mA |
    | Fan       | 2         | 144 mA  |  166 mA |
    | LED App   | 4         |  50 mA  |  110 mA |
    | INA 223	| 2			|		  |   10 mA |
    | Total     |           |         | 1046 mA |
    */

    float INA_R_SHUNT = 0.100;
    float INA_I_MAX_IN  = 1.1;
    float INA_I_MAX_OUT = 0.3;

    ESP_ERROR_CHECK(ina233_init(i2c_num, ina_grid));
    ESP_ERROR_CHECK(ina233_init(i2c_num, ina_pv));
    ESP_ERROR_CHECK(ina233_setCalibration(ina_grid, INA_R_SHUNT, INA_I_MAX_IN));
    ESP_ERROR_CHECK(ina233_setCalibration(ina_pv  , INA_R_SHUNT, INA_I_MAX_OUT));
    return ESP_OK;
}


/**
 * @brief Init additional devices
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_devices()
{
    ESP_ERROR_CHECK(tmp100_init(i2c_num,tmp_in));       // TMP100 temperature sensor Inside
    ESP_ERROR_CHECK(tmp100_init(i2c_num,tmp_out));      // TMP100 temperature sensor Outside
    ESP_ERROR_CHECK(pca9536_init(i2c_num,gpio_x));      // PCA9536 GPIO expander

    // Update initial temp
    entity.temp.outside = tmp100_readTemp(tmp_out);
    entity.temp.inside  = tmp100_readTemp(tmp_in);
    entity.temp.target  = entity.temp.outside;

    // Update initial GPIOx status
    pca9536_set_gpio_level(0,0);
    pca9536_set_gpio_level(1,0);
    pca9536_set_gpio_level(2,0);
    pca9536_set_gpio_level(3,0);

    return ESP_OK;
}


/**
 * @brief Init timer for 7SEG Display refresh task
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_display()
{
    set_display_value(entity.temp.inside);

    // Create display refresh task
    display_timer_alarm = (uint64_t) display_timer_ms * TIMER_SCALE / 1000ULL;
    xTaskCreate(display_refresh_task, "display_refresh_task", 2048, NULL, 4, &display_task_handle);

	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_1, &cfg));
	ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_1));
	ESP_ERROR_CHECK(timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL));
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_1, display_timer_alarm));
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_1));
	ESP_ERROR_CHECK(timer_isr_register(TIMER_GROUP_0, TIMER_1, display_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_1));

    return ESP_OK;
}


/**
 * @brief Init timer for main entity task
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
	entity_timer_alarm = (uint64_t) entity_timer_ms * TIMER_SCALE / 1000ULL;

	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
	ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_isr_register(TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

	ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);
    
	return ESP_OK;
}


/**
 * @brief Init entity
 *        Initialize the entity-specific submodules and start the activity tasks
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t house_init_entity()
{
    ESP_LOGI(TAG, "Init House entity");

    // Set default LED intensity
    entity.led_intensity = 0.5;

    init_gpio();
    init_pwm();
    init_owb();
    init_i2c();
    init_spi();
    init_touch();
    init_telemetry();
    init_devices();
    init_display();

    // Entity main tasks
	xTaskCreate(entity_main_tasks, "entity_main_task", 2048, NULL, 4, &entity_task_handle);
	ESP_ERROR_CHECK(init_tasks_timer());

    return ESP_OK;
}

//
//------------------------------------------------------------EXTERNAL FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t house_http_resp(char* str)
{
    sprintf(str, "power|%.2f;load|%.2f;temp|%.1f",
            entity.power_out.power/1000,
            entity.power_out.current/1000.0,
            entity.temp.inside);

	return ESP_OK;
}



/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t house_http_read(char *str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
    }

    if (msg_unmarshall(str, "temp", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=-5.0) && (val<=5.0)){
                remote_override = 255;      // 25 s override
                remote_target = val;
                ESP_LOGD(TAG, "TEMPERATURE");
            }
            return ESP_OK;
    }

    return ESP_FAIL;
}


/**
 * @brief MQTT init topics
 *        Subscribe to entity-specific topics
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t house_mqtt_init(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_service, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_service);

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_sub, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_sub);

    return ESP_OK;
}


/**
 * @brief MQTT post
 *        Callback for periodic publishing updates via MQTT
 * 
 * @param[in] client Pointer to MQTT client handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t house_mqtt_post(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;
    char data[256];
    sprintf(data,"%10s: GRID: %7.1f mA [%4.2f V]\tPV: %7.1f mA [%4.2f V]\tTemp: %4.1f [Fix] %4.1f [In] %4.1f [Out]°C",TAG,entity.power_in.current,entity.power_in.voltage,entity.power_out.current,entity.power_out.voltage,entity.temp.target,entity.temp.inside,entity.temp.outside);
    esp_mqtt_client_publish(mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
    return ESP_OK;
}


/**
 * @brief MQTT read
 *        Callback for reading subscribed topics via MQTT
 * 
 * @param[in] event Pointer to MQTT event handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t house_mqtt_read(void *event)
{
    mqtt_event = (esp_mqtt_event_handle_t)event;

    char* data = (char*)malloc(mqtt_event->data_len+1);
    memcpy(data,mqtt_event->data,mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len+1);
    memcpy(topic,mqtt_event->topic,mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic,mqtt_topic_service)){
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val,TAG)){
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return ESP_OK;
            }

        if (msg_unmarshall(data, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
        }
    }

    return ESP_OK;
}