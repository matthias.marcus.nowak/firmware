# House
The House represents a common residential prosumer, in which the power absorbed from the grid is mainly due to household appliances. The ambient can be thermo-regulated, allowing both heating and cooling. The current temperature is visualized on a 3-digit LED display, but it switches to target temperature when using the increment/decrement touch buttons. A PV-cell on the roof generates additional power, which can freely flow back into the grid if in surplus.

## Main code
### Status structure
```c
struct House
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     setpoint_imax;      // Current output level
    temp_t      temp;               // Temperature monitor
    float       pv_intensity;       // Normalized light intensity
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
    telemetry_t power_out;          // Output telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> touch[Touch]
    touch --> telemetry[Telemetry]
    telemetry --> devices[Devices]
    devices --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> mix[Update Energy Mix]
    mix --> touch[Detect Touch]
    touch --> temp[Update Temperature]
    temp --> display[Update Display]
    display --> control[Control Appliances]
    control --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/house**
    - Telemetry
        - *Format*: `House: GRID: <I> mA [<V> V]   PV: <I> mA [<V> V]    Temp: <Tf> [Fix] <Ti> [In] <To> [Out]°C`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the GRID and PV meter. Current temperature target is **Tf**, **Ti** and **To** are internal and external respectively<br/>(*eg.* `House: GRID: 121.7 mA [3.36 V]   PV: 34.3 mA [3.38 V]    Temp: 25.5 [Fix] 24.8 [In] 22.3 [Out]°C`)

### Subscribe
- **/legos/house/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|House`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>;pv|<Wext>;temp|<T>`
        - *Description*: Log telemetry data, **W** is the input power measured at the meter. Power generated from pv is **Wext**. Internal temperature is **T**.(*eg.* `power|0.32;chp|0.5;temp|25.3`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Temperature
        - Format: `target|<val>`
        - Description: Set temperature target with respect to internal temperature to **val** [-5.0 to 5.0]
### WEB PAGE
[<img src="docs/web_page.png"  width="120" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/house/house.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/house/house.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/house/house.md)