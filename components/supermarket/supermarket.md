# Supermarket
The supermarket represents a common commercial prosumer, in which the power absorbed from the grid is mainly used for lighting and thermo-regulation, in which an emulated thermal system fills a storage element of hot water and use it when needed. A combined heat and power (CHP) system allows to extract the required energy from burning gas as alternative to using energy from the grid. The mixing ratio of the two sources is defined by an algorithm based on their market cost in €/kWh. The cost of the selected source is visualized on a 3-digit LED display by toggling the selection touch button, whereas the increment/decrement touch buttons can be used to change the cost. If more power than required is generated, it can freely flow back into the grid.

## Main code
### Status structure
```c
struct Supermarket
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     setpoint_imax;      // Current output level
    price_t     price;              // Energy market cost
    float       source_mix;         // Percentage from grid
    float       storage;            // Thermal storage level
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
    telemetry_t power_out;          // Output telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> touch[Touch]
    touch --> telemetry[Telemetry]
    telemetry --> display[Display]
    display --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> load[Update thermal load]
    load --> touch[Detect Touch]
    touch --> display[Update Display]
    display --> mix[Update energy mix]
    mix --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/supermarket**
    - Telemetry
        - *Format*: `Supermarket: GRID: <I> mA [<V> V] (<val> €)  GAS: <I> mA [<V> V] (<val> €)`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the GRID and CHP meter. Current cost in cent/kWh is **val**<br/>(*eg.* `Supermarket: GRID: 121.7 mA [3.36 V] (20.1 €)   GAS: 34.3 mA [3.38 V] (19.7 €)`)

### Subscribe
- **/legos/supermarket/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|Supermarket`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>;chp|<Wext>`
        - *Description*: Log telemetry data, **W** is the input power measured at the meter. Power generated from gas is **Wext**.(*eg.* `power|0.32;chp|0.5`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Delta
        - Format: `delta|<val>`
        - Description: Set gas-to-electricity cost delta to **val** [-2 to 2]
### WEB PAGE
[<img src="docs/web_page.png"  width="119" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/supermarket/supermarket.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/supermarket/supermarket.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/supermarket/supermarket.md)