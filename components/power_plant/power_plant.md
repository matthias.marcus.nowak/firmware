# Power Plant
The Power Plant represents a traditional power producer from fossil fuel combustion. It fixes the voltage of the nominal voltage of the entities adopting a voltage source configuration, able to erogate up to 4 A when the output current limit is set to max. The current power load is visualized by the tower using a trichromatic color scale: green-yellow (0 % to 50 %) and yellow-red (50 % to 100 %). A slider on the side can be used for varying the output current limit.

## Main code
### Status structure
```c
struct Powerplant
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     setpoint_vbus;      // Voltage output level
    uint8_t     setpoint_imax;      // Current output level
    float       ldo_temp;           // LDO temperature
    telemetry_t power_out;          // Output telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> touch[Touch]
    touch --> telemetry[Telemetry]
    telemetry --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> monitor[Monitor Power]
    monitor --> touch[Detect Touch]
    touch --> control[Control Bus]
    control --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/power_plant**
    - Telemetry
        - *Format*: `PowerPlant: <I> mA [<V> V]`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter.<br/>(*eg.* `PowerPlant: 2121.7 mA [3.36 V]`)
- **/legos/service/cmd**
    - SCADA
        - Format: `fuel|<val>`
        - Description: Log the total current output normalized to 4 A

### Subscribe
- **/legos/power_plant/cmd**
    - VBUS
        - Format: `vbus|<val>`
        - Description: Set the bus voltage to **val** [0 to 255], equivalent approximately to [3 V to 3.8 V]
    - IMAX
        - Format: `imax|<val>`
        - Description: Set the max bus current to **val** [0 to 255], equivalent approximately to [2 A to 3.8 A]
- **/legos/service/cmd**
    - Update
        - Format: `update|PowerPlant`
        - Description: Update firmware

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>;load|<val>`
        - *Description*: Log telemetry data, **W** is the output power measured at the meter. Current load **val** is in the range [0.0 to 1.0](*eg.* `power|0.32;load|0.5`)
### POST
- **/post**
    - Max load
        - Format: `max_load|<val>`
        - Description: Set max power load to **val** [0.0 to 1.0]
### WEB PAGE
[<img src="docs/web_page.png"  width="121" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/power_plant/power_plant.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/power_plant/power_plant.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/power_plant/power_plant.md)