# MFRC522
## Description
This component provides an ESP32 library for interfacing interfacing MFRC522 RFID shield. The library is equivalent to [ESPIDF-INTERFACE-MFRC522](https://github.com/anuragmandal/ESPIDF-INTERFACE-MFRC522) with the addition of two methods.

## Main code
### Additional Methods
```c
// Write RFID storage
void PICC_StoreSector(spi_device_handle_t spi,Uid *uid, uint8_t BlockAddr, uint8_t *buffer, MIFARE_Key *key);

// Init MFRC522 RFID shield
void Init_RFID_shield(spi_device_handle_t spi);
```

## Documents
Datasheet: [MFRC522](https://www.nxp.com/docs/en/data-sheet/MFRC522.pdf)
