//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _SKYSCRAPER_H_
#define _SKYSCRAPER_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "ms4.h"
#include "legos_common.h"


// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0      // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1      // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_5      // I2C Pins
#define PIN_SCL         GPIO_NUM_18

#define PIN_EVENT      	GPIO_NUM_22     // Ambimate Presence Trigger

#define PIN_SCADA_FAULT	GPIO_NUM_4      // Leds for Monitoring
#define PIN_SCADA_LOAD	GPIO_NUM_16
#define PIN_SCADA_SOLAR	GPIO_NUM_17
#define PIN_SCADA_FUEL	GPIO_NUM_19
#define PIN_SCADA_WIND 	GPIO_NUM_21

#define PIN_CYB_SECURITY GPIO_NUM_14    // Leds for Cyber Security
#define PIN_CYB_BREACH	GPIO_NUM_25
#define PIN_CYB_ATTACK	GPIO_NUM_27

#define PIN_HOUSE_TV	GPIO_NUM_26

#define PIN_LIGHT_INT   GPIO_NUM_33     // Lighting LEDs
#define PIN_LIGHT_EXT   GPIO_NUM_32

#define IRQ_INA         GPIO_NUM_22     // INA233 Interrupt Input

#define PIN_AHU_FLAP    GPIO_NUM_15     // Position of Flap
#define PIN_AHU_FAN     GPIO_NUM_2      // Air speed
#define PIN_AHU_HEAT    GPIO_NUM_12     // Heating

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum pwm_control{
    PWM_HOUSE_TV = 0,
    PWM_LIGHT_INT,
    PWM_LIGHT_EXT,
    PWM_AHU_FLAP,
    PWM_AHU_FAN,
    PWM_AHU_HEAT,
    PWM_CONTROL_MAX
};

enum led_monitor_channels{
    LED_SCADA_FAULT = PWM_CONTROL_MAX,
    LED_SCADA_LOAD,
    LED_SCADA_SOLAR,
    LED_SCADA_FUEL,
    LED_SCADA_WIND,
    LED_CYB_SECURITY,
    LED_CYB_BREACH,
    LED_CYB_ATTACK,
    LED_MONITOR_MAX,
};

enum servo_pos{
    SERVO_POS_OPEN  = 490,              // Servo position Open
    SERVO_POS_CLOSE = 800,              // Servo position Close
};

// ---------- Entity Definitions ----------

/**
 * @brief SCADA system status
 */
typedef struct
{
    float load;
    float solar;
    float fuel;
    float wind;
    bool  fault;
    bool  attack;
    bool  breach;
    bool  security;
} scada_t;

/**
 * @brief HVAC system status
 */
typedef struct
{
    float fan;
    float heat;
    bool flap;
} hvac_t;

/**
 * @brief Entity status structure
 */
struct Skyscraper
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     event;              // Sensor event
    scada_t     scada;              // SCADA status
    hvac_t      hvac;               // HVAC status
    ms4_t       ms4;                // Sensor readings
    bool        match;              // Sport match status
    float       led_intensity;      // Normalized global intensity
    telemetry_t power_in;           // Input telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/skyscraper";
static const char mqtt_topic_sub[] = "/legos/skyscraper/cmd";


/**
 * @brief Entity HTML webpage
 */
static const char html_webpage[] = "";

#ifdef __cplusplus
extern "C" {
#endif

esp_err_t skyscraper_init_entity();
esp_err_t skyscraper_http_resp(char *str);
esp_err_t skyscraper_http_read(char *str);
esp_err_t skyscraper_mqtt_init(void *client);
esp_err_t skyscraper_mqtt_post(void *client);
esp_err_t skyscraper_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() skyscraper_init_entity()
#define http_resp(x) skyscraper_http_resp(x)
#define http_read(x) skyscraper_http_read(x)
#define mqtt_init(x) skyscraper_mqtt_init(x)
#define mqtt_post(x) skyscraper_mqtt_post(x)
#define mqtt_read(x) skyscraper_mqtt_read(x)

#endif //_SKYSCRAPER_H_