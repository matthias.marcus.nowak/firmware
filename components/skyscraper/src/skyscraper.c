//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "skyscraper.h"
#include "esp_log.h"
#include "mqtt_client.h"

static const char *TAG = "Skyscraper";              // Logging tag

static i2c_port_t i2c_num = I2C_NUM_1;              // I2C port
static uint8_t ina_grid = INA233_ADDRESS_40;        // I2C address INA233 Bus
static uint8_t ambimate = MS4_ADDRESS_2A;           // I2C address EnviSens

static uint16_t entity_timer_ms = 250;              // Main-tasks Thread interval
static uint64_t entity_timer_alarm;                 // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;             // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN,       // Timer configuration
                              TIMER_PAUSE,
                              TIMER_INTR_LEVEL,
                              TIMER_COUNT_UP,
                              TIMER_AUTORELOAD_EN,
                              TIMER_DIVIDER};
static uint8_t remote_override = 0;
static float remote_lights = 0;
static esp_mqtt_client_handle_t mqtt_client;        // MQTT client handle
static esp_mqtt_event_handle_t mqtt_event;          // MQTT event handle

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[LED_MONITOR_MAX] =
{
	{
        .channel = 0,
        .duty = 0,
        .gpio_num = PIN_HOUSE_TV,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LIGHT_INT,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LIGHT_EXT,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 3,
        .duty = 0,
        .gpio_num = PIN_AHU_FLAP,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 4,
        .duty = 0,
        .gpio_num = PIN_AHU_FAN,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 5,
        .duty = 0,
        .gpio_num = PIN_AHU_HEAT,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
    {
        .channel = 0,
        .duty = 0,
        .gpio_num = PIN_SCADA_FAULT,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
    {
        .channel = 1,
        .duty = 0,
        .gpio_num = PIN_SCADA_LOAD,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
    {
        .channel = 2,
        .duty = 0,
        .gpio_num = PIN_SCADA_SOLAR,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
    {
        .channel = 3,
        .duty = 0,
        .gpio_num = PIN_SCADA_FUEL,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
    {
        .channel = 4,
        .duty = 0,
        .gpio_num = PIN_SCADA_WIND,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
    {
        .channel = 5,
        .duty = 0,
        .gpio_num = PIN_CYB_SECURITY,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
    {
        .channel = 6,
        .duty = 0,
        .gpio_num = PIN_CYB_BREACH,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
    {
        .channel = 7,
        .duty = 0,
        .gpio_num = PIN_CYB_ATTACK,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer =
    {
        .duty_resolution = LEDC_TIMER_13_BIT,   // PWM duty-cycle resolution
        .freq_hz = 50,                          // PWM frequency
        .speed_mode = PWM_LS_MODE,              // PWM timer mode
        .timer_num = PWM_LS_TIMER,              // PWM timer index
        .clk_cfg = LEDC_AUTO_CLK,               // Auto select the source clock
};

//
//------------------------------------------------------------SERVICE FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 * 
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty, fade_time_ms);
    ledc_fade_start(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG,"PWM fade\tch:%d@%d in %d ms",ch,duty,fade_time_ms);
}


/**
 * @brief Set PWM channel dutycycle
 * 
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * 
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG,"PWM switch\tch:%d@%d",ch,duty);
}


/**
 * @brief Read INA233 Telemetry
 * 
 * @param[in] addr Addres of the device on the I2C bus
 * 
 * @return void
 */
void read_telemetry(uint8_t addr)
{
    entity.power_in.voltage = ina233_getBusVoltage_V(addr);
    entity.power_in.current = ina233_getCurrent_mA(addr);
    entity.power_in.power = ina233_getPower_mW(addr);
    ESP_LOGD(TAG,"Telemetry\tbus: %f V,shunt: %f mA, pwr: %f mW",entity.power_in.voltage,entity.power_in.current,entity.power_in.power);
}


/**
 * @brief Update SCADA indicators
 * 
 * @param[in] led_intensity Normalized LED intensity
 * 
 * @return void
 */
void update_scada_indicator(float led_intensity)
{
    pwm_switch(LED_SCADA_FAULT, entity.scada.fault ? led_intensity : LED_INTENSITY_0);
    pwm_switch(LED_SCADA_LOAD,  entity.scada.load*led_intensity);
    pwm_switch(LED_SCADA_SOLAR, entity.scada.solar*led_intensity);
    pwm_switch(LED_SCADA_FUEL,  entity.scada.fuel*led_intensity);
    pwm_switch(LED_SCADA_WIND,  entity.scada.wind*led_intensity);

    pwm_switch(LED_CYB_SECURITY,entity.scada.security ? led_intensity : LED_INTENSITY_0);
    pwm_switch(LED_CYB_BREACH,  entity.scada.breach   ? led_intensity : LED_INTENSITY_0);
    pwm_switch(LED_CYB_ATTACK,  entity.scada.attack   ? led_intensity : LED_INTENSITY_0);
}


/**
 * @brief Update HVAC system
 * 
 * @param[in] led_intensity Normalized LED intensity
 * 
 * @return void
 */
void update_hvac(float led_intensity)
{
    // TODO
}


/**
 * @brief Entity main task timer ISR
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
	// Clear interrupt
	TIMERG0.int_clr_timers.t0 = 1;

	// Restart timer
	timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

	// Notify task
	xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
	portYIELD_FROM_ISR();
}


/**
 * @brief Entity main task
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void entity_main_tasks(void* arg)
{
    // Initialize manufacturing line count
    uint8_t step_count = 0;
    entity.hvac.flap = SERVO_POS_OPEN;
    entity.hvac.fan = 1;
    entity.scada.security = true;
    uint8_t cycle = 0;

        entity.ms4.status = true;

	while (1)
	{
		// Wait timer
		xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

		// Get data
		read_telemetry(ina_grid);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity*LED_INTENSITY_100;

        // Update SCADA status
        update_scada_indicator(led_intensity);

        // Update AmbiMate sensors
        ms4_readAll();

        // Update building lights
        pwm_switch(PWM_LIGHT_EXT, led_intensity);
        pwm_switch(PWM_HOUSE_TV , entity.match ? led_intensity : LED_INTENSITY_0);
        if (remote_override){
            pwm_switch(PWM_LIGHT_INT, remote_lights*LED_INTENSITY_100);
            remote_override--;
        } else {
            pwm_switch(PWM_LIGHT_INT, entity.ms4.status ? led_intensity : LED_INTENSITY_0);
        }

        // Control HVAC
        pwm_switch(PWM_AHU_FAN,  entity.hvac.fan *LED_INTENSITY_100);
        pwm_switch(PWM_AHU_HEAT, entity.hvac.heat*LED_INTENSITY_100);
        pwm_switch(PWM_AHU_FLAP, entity.hvac.flap ? SERVO_POS_OPEN : SERVO_POS_CLOSE);

        if (cycle == 199)
            cycle = 0;
        else
            cycle++;
            
        if (cycle == 0)
            pwm_fade(PWM_AHU_FLAP, SERVO_POS_OPEN, entity_timer_ms*50);
        if (cycle == 100)
            pwm_fade(PWM_AHU_FLAP, SERVO_POS_CLOSE, entity_timer_ms*50);
        
	}
	vTaskDelete(NULL);
}

//
//------------------------------------------------------------INIT FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Init GPIO
 * 
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{

    return ESP_OK;
}


/**
 * @brief Init PWM channels
 * 
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer0 for low speed channels
    // Prepare and set configuration of timer1 for high speed channels
    pwm_timer.freq_hz = 5000;
    pwm_timer.speed_mode = PWM_HS_MODE;
    pwm_timer.timer_num = PWM_HS_TIMER;
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer1 for high speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_MONITOR_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0));                                     // Initialize fade service
}


/**
 * @brief Init 1-Wire interface
 * 
 *        Initialize the 1-Wire interface and detect the 64-bit ID eeprom connected to
 *        the bus A and B
 * @note  Return ESP_FAIL in case of branch not terminated on a node and trigger a reset
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t init_owb()
{
    // esp_log_level_set("owb", ESP_LOG_INFO);
    // esp_log_level_set("owb_rmt", ESP_LOG_INFO);

    // Create a 1-Wire bus, using the RMT timeslot driver
    OneWireBus *owb;
    owb_rmt_driver_info rmt_driver_info;
    owb = owb_rmt_initialize(&rmt_driver_info, PIN_EEPROM, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true); // enable CRC check for ROM code
    owb_use_parasitic_power(owb, true);

    // Find connected device
    OneWireBus_SearchState search_state = {0};
    bool found = false;
    owb_search_first(owb, &search_state, &found);
    OneWireBus_ROMCode device_rom_code = search_state.rom_code;
    
    ESP_LOGD(TAG, "Node ID:");
    if (found)
    {
        esp_log_buffer_hex_internal(TAG, device_rom_code.bytes, 8, ESP_LOG_DEBUG);
        memcpy(entity.id, device_rom_code.bytes, 8);
        return ESP_OK;
    }
    else
    {
        ESP_LOGD(TAG, "not valid");
        return ESP_FAIL;
    }
}


/**
 * @brief Init the I2C interface
 * 
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
esp_err_t init_i2c()
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    ESP_LOGD(TAG, "sda_io_num %d", PIN_SDA);

    conf.sda_io_num = PIN_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "scl_io_num %d", PIN_SCL);

    conf.scl_io_num = PIN_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "clk_speed %d", I2C_MASTER_FREQ_HZ);

    conf.clk_flags = 0;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    ESP_LOGD(TAG, "i2c_param_config %d", conf.mode);
    ESP_ERROR_CHECK(i2c_param_config(i2c_num, &conf));

    ESP_LOGD(TAG, "i2c_driver_install %d", i2c_num);
    return (i2c_driver_install(i2c_num, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0));
}


/**
 * @brief Init the INA233 power monitor
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_telemetry()
{
    /*
    VBus Rail
    | Part      | Count     | Typical | Max     |
    | --------- | :-------: | ------: | ------: |
    | ESP32     |  1        | 160 mA  |  260 mA |
    | Fan       |  1        |         |   83 mA |
    | INA 223	|  1    	|		  |    5 mA |
    | IAQ       |  1        |         |   33 mA |
    | Servo     |  1 		|         |  		|
    | Heating   |  1        |         |  110 mA |
    | LED SCADA |  5        |  25 mA  |   50 mA |
    | LED EXT   | 12        | 120 mA  |  240 mA |
    | LED INT   |  3        |  30 mA  |   60 mA |
    | LED TV    |  1        |  10 mA  |   20 mA |
    | Total     |           |         |  861 mA |
    */

    float INA_R_SHUNT = 0.100;
    float INA_I_MAX = 1.0;

    ESP_ERROR_CHECK(ina233_init(i2c_num, ina_grid));
    ESP_ERROR_CHECK(ina233_setCalibration(ina_grid, INA_R_SHUNT, INA_I_MAX));
    return ESP_OK;
}


/**
 * @brief Init Ambimate IAQ sensor
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_ambimate()
{
    ESP_ERROR_CHECK(ms4_init(i2c_num, ambimate, &entity.ms4));
    ms4_readAll();
    return ESP_OK;
}


/**
 * @brief Init timer for main entity task
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
	entity_timer_alarm = (uint64_t) entity_timer_ms * TIMER_SCALE / 1000ULL;

	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
	ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_isr_register(TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

	ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

	return ESP_OK;
}


/**
 * @brief Init entity
 *        Initialize the entity-specific submodules and start the activity tasks
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t skyscraper_init_entity()
{
    // Set default LED intensity
    entity.led_intensity = 0.5;

    init_gpio();
    init_pwm();
    init_owb();
    init_i2c();
    init_telemetry();
    ESP_ERROR_CHECK(init_ambimate());

    // Entity main tasks
	xTaskCreate(entity_main_tasks, "entity_main_task", 2048, NULL, 4, &entity_task_handle);
	ESP_ERROR_CHECK(init_tasks_timer());

    return ESP_OK;
}

//
//------------------------------------------------------------EXTERNAL FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t skyscraper_http_resp(char* str)
{
    sprintf(str, "power|%.2f",
            entity.power_in.power/1000);

	return ESP_OK;
}



/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t skyscraper_http_read(char *str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
        }

    if (msg_unmarshall(str, "lights", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                remote_lights = val;
                remote_override = 150;
                ESP_LOGD(TAG, "LIGHTS");
            }
            return ESP_OK;
        }

    return ESP_FAIL;
}


/**
 * @brief MQTT init topics
 *        Subscribe to entity-specific topics
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t skyscraper_mqtt_init(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_service, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_service);

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_sub, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_sub);

    return ESP_OK;
}


/**
 * @brief MQTT post
 *        Callback for periodic publishing updates via MQTT
 * 
 * @param[in] client Pointer to MQTT client handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t skyscraper_mqtt_post(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;
    char data[256];
    sprintf(data,"%10s: %7.1f mA [%4.2f V]",TAG,entity.power_in.current,entity.power_in.voltage);
    esp_mqtt_client_publish(mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
    return ESP_OK;
}


/**
 * @brief MQTT read
 *        Callback for reading subscribed topics via MQTT
 * 
 * @param[in] event Pointer to MQTT event handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t skyscraper_mqtt_read(void *event)
{
    mqtt_event = (esp_mqtt_event_handle_t)event;

    char* data = (char*)malloc(mqtt_event->data_len+1);
    memcpy(data,mqtt_event->data,mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len+1);
    memcpy(topic,mqtt_event->topic,mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic,mqtt_topic_service)){
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val,TAG)){
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return ESP_OK;
            }

        if (msg_unmarshall(data, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
        }
        if (msg_unmarshall(data, "fuel", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0))
                entity.scada.fuel = val;
            ESP_LOGD(TAG, "FUEL");
            return ESP_OK;
        }

        if (msg_unmarshall(data, "solar", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0))
                entity.scada.solar = val;
            ESP_LOGD(TAG, "SOLAR");
            return ESP_OK;
        }

        if (msg_unmarshall(data, "wind", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0))
                entity.scada.wind = val;
            ESP_LOGD(TAG, "WIND");
            return ESP_OK;
        }

        if (msg_unmarshall(data, "stadium", &msg_payload)){
            uint8_t val = atoi(msg_payload.val);
            if (val==2)
                entity.match = true;
            ESP_LOGD(TAG, "MATCH");
            return ESP_OK;
        }
    }

    return ESP_OK;
}