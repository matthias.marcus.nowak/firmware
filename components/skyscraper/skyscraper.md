# Skyscraper
The Skyscraper represents a common aggregated residential consumer. The building hosts a SCADA center for monitoring the overall status of the entities, a cyber-security center for monitoring the status of cyber-attacks and data-breach affecting the infrastructure. The building is provided with a HVAC system that takes of air recirculation, heating and monitors the indoor air quality (IAQ).

## Main code
### Status structure
```c
struct Skyscraper
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     event;              // Sensor event
    scada_t     scada;              // SCADA status
    hvac_t      hvac;               // HVAC status
    ms4_t       ms4;                // Sensor readings
    bool        match;              // Sport match status
    float       led_intensity;      // Normalized global intensity
    telemetry_t power_in;           // Input telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> telemetry[Telemetry]
    telemetry --> ms4[Sensors]
    ms4 --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> scada[Update SCADA]
    scada --> sensor[Read Sensors]
    sensor --> hvac[Update HVAC]
    hvac --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/skyscraper**
    - Telemetry
        - *Format*: `Skyscraper: GRID: <I> mA [<V> V]`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter<br/>(*eg.* `Skyscraper: 121.7 mA [3.36 V]`)

### Subscribe
- **/legos/skyscraper/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|Skyscraper`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Fuel
        - Format: `fuel|<val>`
        - Description: Set the intensity of the fuel indicator to **val** [0.0 to 1.0]
    - Solar
        - Format: `solar|<val>`
        - Description: Set the intensity of the solar indicator to **val** [0.0 to 1.0]
    - Wind
        - Format: `wind|<val>`
        - Description: Set the intensity of the wind indicator to **val** [0.0 to 1.0]
    - Stadium
        - Format: `stadium|<val>`
        - Description: Set the status to match **val** [2]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>`
        - *Description*: Log telemetry data, **W** is the input power measured at the meter<br/>(*eg.* `power|0.32`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Lights
        - Format: `lights|<val>`
        - Description: Set internal lights intensity to **val** [0.0 to 1.0]
### WEB PAGE
[<img src="docs/web_page.png"  width="122" height="200">](docs/web_page.png)

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/skyscraper/skyscraper.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/skyscraper/skyscraper.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/skyscraper/skyscraper.md)