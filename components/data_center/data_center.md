# Data Center
The Data Center represents one of the critical infrastructure for IT operations, which must provide uninterrupted operation even during a power outage. Can detects possible data-breach due to unauthorized access to the LEGOS project repository stored inside.

## Main code
### Status structure
```c
struct DataCenter
{
    uint8_t     id[8];              // Unique port identifier
    bool        power_source;       // Power source (0 GRID, 1 UPS)
    float       led_intensity;      // Normalized global brightness
    float       server_load;        // Normalized workload
    telemetry_t power_in;           // Input telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> telemetry[Telemetry]
    telemetry --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> load[Update Workload]
    load --> status[Update Servers]
    status --> source[Detect Source]
    source --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/data_center**
    - Telemetry
        - *Format*: `DataCenter: <I> mA [<V> V]   Source: <val>`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter. Source **val** can be either *GRID* or *UPS*<br/>(*eg.* `DataCenter: 121.7 mA [3.36 V]   Source: UPS`)

### Subscribe
- **/legos/data_center/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|DataCenter`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>;source|<val>`
        - *Description*: Log telemetry data, **W** is the input power measured at the meter. Source **val** can be either *grid* or *ups*<br/>(*eg.* `power|0.32;source|grid`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
### WEB PAGE
[<img src="docs/web_page.png"  width="122" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/data_center/data_center.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/data_center/data_center.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/data_center/data_center.md)