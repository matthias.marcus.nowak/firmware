//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _DATA_CENTER_H_
#define _DATA_CENTER_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0  // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1  // Data communication
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_5  // I2C Pins
#define PIN_SCL         GPIO_NUM_18

#define PIN_MISO        GPIO_NUM_19 // SPI Pins
#define PIN_MOSI        GPIO_NUM_22
#define PIN_CLK         GPIO_NUM_21
#define PIN_CS          GPIO_NUM_23

#define PIN_LOAD        GPIO_NUM_32 // LED Servers
#define PIN_DATA_1      GPIO_NUM_12
#define PIN_DATA_2      GPIO_NUM_14
#define PIN_DATA_3      GPIO_NUM_33
#define PIN_DATA_4      GPIO_NUM_27
#define PIN_DATA_5      GPIO_NUM_26
#define PIN_DATA_6      GPIO_NUM_25

#define PIN_UPS_1       GPIO_NUM_13 // LED UPS
#define PIN_UPS_2       GPIO_NUM_2
#define PIN_UPS_3       GPIO_NUM_4

#define PIN_SRC         GPIO_NUM_15 // Power source selection (0 GRID, 1 UPS)

#define IRQ_INA_BUS     GPIO_NUM_17 // INA233 Interrupt

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum led_server{
    LED_SERVER_LOAD = 0,            // GPIO_NUM_32  Server Status LED (white)
    LED_SERVER_DATA_1,              // GPIO_NUM_12  Server LEDs (blue)
    LED_SERVER_DATA_2,              // GPIO_NUM_14
    LED_SERVER_DATA_3,              // GPIO_NUM_33
    LED_SERVER_DATA_4,              // GPIO_NUM_27
    LED_SERVER_DATA_5,              // GPIO_NUM_26
    LED_SERVER_DATA_6,              // GPIO_NUM_25
    LED_SERVER_MAX,
};

enum led_ups{
    LED_UPS_1 = LED_SERVER_MAX,     // GPIO_NUM_13  UPS LEDs
    LED_UPS_2,                      // GPIO_NUM_2
    LED_UPS_3,                      // GPIO_NUM_4
    LED_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct DataCenter
{
    uint8_t     id[8];              // Unique port identifier
    bool        power_source;       // Power source (0 GRID, 1 UPS)
    float       led_intensity;      // Normalized global brightness
    float       server_load;        // Normalized load
    telemetry_t power_in;           // Input telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/data_center";
static const char mqtt_topic_sub[] = "/legos/data_center/cmd";


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t datacenter_init_entity();
esp_err_t datacenter_http_resp(char *str);
esp_err_t datacenter_http_read(char *str);
esp_err_t datacenter_mqtt_init(void *client);
esp_err_t datacenter_mqtt_post(void *client);
esp_err_t datacenter_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() datacenter_init_entity()
#define http_resp(x) datacenter_http_resp(x)
#define http_read(x) datacenter_http_read(x)
#define mqtt_init(x) datacenter_mqtt_init(x)
#define mqtt_post(x) datacenter_mqtt_post(x)
#define mqtt_read(x) datacenter_mqtt_read(x)

#endif //_DATA_CENTER_H_