//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _SUBSTATION_H_
#define _SUBSTATION_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"

#include "owb.h"
#include "owb_rmt.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0  // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1  // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define FAULT_1        	GPIO_NUM_17 // Faults
#define FAULT_2         GPIO_NUM_4
#define FAULT_3			GPIO_NUM_2
#define FAULT_4			GPIO_NUM_12
#define FAULT_5			GPIO_NUM_14
#define FAULT_6			GPIO_NUM_27

#define BUS_EN_1        GPIO_NUM_16
#define BUS_EN_2        GPIO_NUM_19
#define BUS_EN_3        GPIO_NUM_22
#define	BUS_EN_4		GPIO_NUM_13
#define BUS_EN_5		GPIO_NUM_32
#define BUS_EN_6		GPIO_NUM_25

#define BUS_IRQ_1       GPIO_NUM_18
#define BUS_IRQ_2       GPIO_NUM_21
#define BUS_IRQ_3       GPIO_NUM_23
#define BUS_IRQ_4		GPIO_NUM_35
#define	BUS_IRQ_5		GPIO_NUM_33
#define BUS_IRQ_6		GPIO_NUM_26

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum pwm_channels{
    PWM_FAULT_1 = 0,                // GPIO_NUM_17  Fault
    PWM_FAULT_2,                    // GPIO_NUM_4
    PWM_FAULT_3,                    // GPIO_NUM_2
    PWM_FAULT_4,                    // GPIO_NUM_12
    PWM_FAULT_5,                    // GPIO_NUM_14
    PWM_FAULT_6,                    // GPIO_NUM_27
    PWM_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct Substation
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     bus_linked[6];      // Bus link detected
    uint8_t     bus_status[6];      // Bus fault status
    uint8_t     bus_enable[6];      // Bus enable status
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/substation";
static const char mqtt_topic_sub[] = "/legos/substation/cmd";


/**
 * @brief Entity HTML webpage
 */
static const char html_webpage[] = "";

#ifdef __cplusplus
extern "C" {
#endif

esp_err_t substation_init_entity();
esp_err_t substation_http_resp(char *str);
esp_err_t substation_http_read(char *str);
esp_err_t substation_mqtt_init(void *client);
esp_err_t substation_mqtt_post(void *client);
esp_err_t substation_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() substation_init_entity()
#define http_resp(x) substation_http_resp(x)
#define http_read(x) substation_http_read(x)
#define mqtt_init(x) substation_mqtt_init(x)
#define mqtt_post(x) substation_mqtt_post(x)
#define mqtt_read(x) substation_mqtt_read(x)

#endif //_SUBSTATION_H_