# Substation
The Substation is a key element for the automation of a power grid. It is able to detect the connected branches, detect the fault status from the respective branch telemetry and eventually trip the branch reclosers.

## Main code
### Status structure
```c
struct Substation
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     bus_linked[6];      // Bus link detected
    uint8_t     bus_status[6];      // Bus fault status
    uint8_t     bus_enable[6];      // Bus enable status
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> telemetry[Telemetry]
    telemetry --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> link[Detect bus link]
    link --> wait{{wait Timer}}
    wait --> status[Detect bus status]
    status --> fault{Fault}
    fault -->|yes| flisr[Invoke FLISR]
    flisr --> sleep
    fault -->|no| sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/substation**
    - Status
        - *Format*: `Substation (EN)[FLT]: #x(<ENx>)[<FLTx>]...`
        - *Description*: Log connected bus status, **ENx** and **FLTx** are the enable and fault state of the bus **x**<br/>(*eg.* `Substation (EN)[FLT]: #1(0)[0] #2(1)[0] #3(1)[1] #4(1)[0] #5(0)[0] #6(1)[1]`)

### Subscribe
- **/legos/substation/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|Substation`
        - Description: Update firmware

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `L1|F1;L2|F2...`
        - *Description*: Log telemetry data **L|F**, where **L** is the link status and **F** is the fault status.(*eg.* `1|0;0|0;1|1;0|1;1|1;0|0`)
### POST
- **/post**
    - Delta
        - Format: `fault|<val>`
        - Description: Set fault to branch **val** [0 to 6]
### WEB PAGE
[<img src="docs/web_page.png"  width="119" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/substation/substation.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/substation/substation.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/substation/substation.md)