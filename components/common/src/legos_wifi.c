//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "legos_wifi.h"
#include "esp_log.h"
#include "esp_http_server.h"

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define NETW_CONNECT_BIT BIT0
#define NETW_FAIL_BIT      BIT1

static EventGroupHandle_t netif_event_group;
static esp_netif_ip_info_t ip_info;
static httpd_handle_t server = NULL;
static esp_netif_t * sta_netif = NULL;
static wifi_config_t wifi_config;

static const char *TAG = "Network";


#define IS_FILE_EXT(filename, ext) \
    (strcasecmp(&filename[strlen(filename) - sizeof(ext) + 1], ext) == 0)


/**
 * @brief Set file type
 *        Set HTTP response content type according to file extension
 * 
 * @param[in] req Pointer to http request
 * @param[in] filename Pointer to file name
 * 
 * @return
 *  - ESP_OK : Request success
 *  - ESP_FAIL : Request fail
 */
static esp_err_t set_content_type_from_file(httpd_req_t *req, const char *filename)
{
    if (IS_FILE_EXT(filename, ".pdf")) {
        return httpd_resp_set_type(req, "application/pdf");
    } else if (IS_FILE_EXT(filename, ".html")) {
        return httpd_resp_set_type(req, "text/html");
	} else if (IS_FILE_EXT(filename, ".css")) {
        return httpd_resp_set_type(req, "text/css");
    } else if (IS_FILE_EXT(filename, ".jpeg")) {
        return httpd_resp_set_type(req, "image/jpeg");
    } else if (IS_FILE_EXT(filename, ".png")) {
        return httpd_resp_set_type(req, "image/png");
    } else if (IS_FILE_EXT(filename, ".ico")) {
        return httpd_resp_set_type(req, "image/x-icon");
    }
    /* This is a limited set only */
    /* For any other type always set as plain text */
    return httpd_resp_set_type(req, "text/plain");
}

/**
 * @brief HTTP GET handler
 *        Called upon HTTP request GET
 * 
 * @param[in] req Pointer to http request
 * 
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t server_handler_get(httpd_req_t* req)
{
	ESP_LOGD(TAG, "HTTP GET  %s", req->uri);

	size_t buf_len, chunk_len, path_len = 64;
	char  *buf, path[path_len];

	// Get header value string length and allocate memory
	buf_len = httpd_req_get_hdr_value_len(req, "Host") + 1;
	if (buf_len > 1) {
		buf = (char*)malloc(buf_len);
		/* Copy null terminated value string into buffer */
		if (httpd_req_get_hdr_value_str(req, "Host", buf, buf_len) == ESP_OK)
			ESP_LOGD(TAG, "Found header => Host: %s", buf);
		free(buf);
	}

	// Prepare buffer
	buf_len = 8192;
	buf = (char*)calloc(buf_len, sizeof(char));

	// Handle specific requests
	if (!strcmp(req->uri, "/status")){
		// Call entity response handler
		http_resp(buf);
		httpd_resp_set_type(req, "text/plain");
		httpd_resp_send(req, (const char*)buf, strlen(buf));
	} else {
		// Serve file
		memset(path, 0, path_len);
		strcpy(path, "/spiffs");
		if(!strcmp(req->uri, "/"))
			strcat(path, "/index.html");
		else
			strcat(path, req->uri);

		FILE* fd = fopen(path, "r");
		if (fd == NULL) {
			ESP_LOGE(TAG, "Failed to open %s", path);
			httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "File does not exist");
			return ESP_FAIL;
		}

    	set_content_type_from_file(req, path);

		do {
			// Read file in chunks into the scratch buffer
			chunk_len = fread(buf, 1, buf_len, fd);

			if (chunk_len > 0) {
				// Send the buffer contents as HTTP response chunk
				if (httpd_resp_send_chunk(req, buf, chunk_len) != ESP_OK) {
					fclose(fd);
					ESP_LOGE(TAG, "File sending failed!");
					// Abort sending file
					httpd_resp_sendstr_chunk(req, NULL);
					// Respond with 500 Internal Server Error
					httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to send file");
				return ESP_FAIL;
				}
			}
        // Keep looping till the whole file is sent
    	} while (chunk_len != 0);
		fclose(fd);

		// Respond with an empty chunk to signal HTTP response completion
		httpd_resp_send_chunk(req, NULL, 0);
	}
	free(buf);

	/* After sending the HTTP response the old HTTP request
	 * headers are lost. Check if HTTP request headers can be read now. */
	if (httpd_req_get_hdr_value_len(req, "Host") == 0)
		ESP_LOGD(TAG, "Request headers lost");

	return ESP_OK;
}

/**
 * @brief HTTP POST handler
 *        Called upon HTTP request POST
 * 
 * @param[in] req Pointer to http request
 * 
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t server_handler_post(httpd_req_t* req)
{
	char buf[64];
	int ret = 0;
	int remaining = req->content_len;

	while (remaining > 0) {
		// Read the data for the request
		if ((ret = httpd_req_recv(req, buf, MIN(remaining, sizeof(buf)))) <= 0) {
			if (ret == HTTPD_SOCK_ERR_TIMEOUT) {
				// Retry receiving if timeout occurred
				continue;
			}
			return ESP_FAIL;
		}
		remaining -= ret;
	}
	buf[req->content_len] = '\0';

	ESP_LOGD(TAG, "HTTP POST %s %s", req->uri, buf);
	// Call entity post request handler
	return http_read(buf);
}

/**
 * @brief Start HTTP server
 * 
 * @return
 *  - ESP_OK : Server started successfully
 *  - ESP_FAIL : Server failed to start
 */
esp_err_t server_start()
{
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();
	config.uri_match_fn = httpd_uri_match_wildcard;
	esp_netif_get_ip_info(sta_netif, &ip_info);
	ESP_LOGD(TAG, "Starting HTTP server on " IPSTR ":%d", IP2STR(&ip_info.ip), config.server_port);

	// Start the httpd server	
	if (httpd_start(&server, &config) == ESP_OK) {

		ESP_LOGD(TAG, "Registering URI handlers");

		// URI handler for generic GET requests
		httpd_uri_t uri_get = {
			.uri		= "/*",
			.method		= HTTP_GET,
			.handler	= server_handler_get,
			.user_ctx	= NULL
		};
		httpd_register_uri_handler(server, &uri_get);

		// URI handler for specific POST requests
		httpd_uri_t uri_post = {
			.uri		= "/post",
			.method		= HTTP_POST,
			.handler	= server_handler_post,
			.user_ctx	= NULL
		};
		httpd_register_uri_handler(server, &uri_post);
		return ESP_OK;
	}

	ESP_LOGE(TAG, "Could not start server");
	return ESP_FAIL;
}


/**
 * @brief Stop HTTP server
 * 
 * @return
 *  - ESP_OK : Server stopped successfully
 *  - ESP_ERR_INVALID_ARG : Handle argument is Null
 */
esp_err_t server_stop()
{
	ESP_LOGD(TAG, "Halting HTTP server");
	// Stop the httpd server
	return httpd_stop(server);
}


/**
 * @brief WiFi event handler
 *        Finite State Machine
 * 
 * @param[in] arg Parameters passed to the task
 * @param[in] event_base Event primitive
 * @param[in] event_id Event id
 * @param[in] event_data Event data
 * 
 * @return void
 */
void network_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
	ESP_LOGD(TAG, "Netw EVENT");
	if (event_base == WIFI_EVENT) {
		switch (event_id) {
		case WIFI_EVENT_STA_START:
			ESP_LOGD(TAG, "WiFi Event:\tSTA start");
			esp_wifi_connect();
			break;

		case WIFI_EVENT_STA_CONNECTED:
			ESP_LOGD(TAG, "WiFi Event:\tSTA connected");
			break;

		case WIFI_EVENT_STA_DISCONNECTED:
			ESP_LOGD(TAG, "WiFi Event:\tSTA disconnected");
			esp_wifi_connect();
			xEventGroupClearBits(netif_event_group, NETW_CONNECT_BIT);
			wifi_event_sta_disconnected_t* wifi_event = (wifi_event_sta_disconnected_t*) event_data;
			if (wifi_event->reason == WIFI_REASON_NO_AP_FOUND) {
				// ToDo
			}
			break;

		default:
			break;
		}
	}
	else if(event_base == IP_EVENT) {
		switch (event_id) {
		case IP_EVENT_STA_GOT_IP:{
			esp_netif_dns_info_t dns_info;
			ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
			esp_netif_t *netif = event->esp_netif;

			ESP_LOGD(TAG, "IP Event:\tSTA Connect to Server ");
			ESP_LOGD(TAG, "IP          : " IPSTR, IP2STR(&event->ip_info.ip));
			ESP_LOGD(TAG, "Netmask     : " IPSTR, IP2STR(&event->ip_info.netmask));
			ESP_LOGD(TAG, "Gateway     : " IPSTR, IP2STR(&event->ip_info.ip));
			esp_netif_get_dns_info(netif, 0, &dns_info);
			ESP_LOGD(TAG, "Name Server1: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));
			esp_netif_get_dns_info(netif, 1, &dns_info);
			ESP_LOGD(TAG, "Name Server2: " IPSTR, IP2STR(&dns_info.ip.u_addr.ip4));

			xEventGroupSetBits(netif_event_group, NETW_CONNECT_BIT);
			}
			break;

		default:
			break;
		}
	}
}


esp_err_t init_spiffs(void)
{
    ESP_LOGI(TAG, "Initializing SPIFFS");

    esp_vfs_spiffs_conf_t conf = {
      .base_path = "/spiffs",
      .partition_label = NULL,
      .max_files = 10,
      .format_if_mount_failed = false
    };

    // Use settings defined above to initialize and mount SPIFFS filesystem.
    esp_err_t ret = esp_vfs_spiffs_register(&conf);

    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE(TAG, "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE(TAG, "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE(TAG, "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return ESP_FAIL;
    }

    size_t total = 0, used = 0;
    ret = esp_spiffs_info(NULL, &total, &used);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to get SPIFFS partition information (%s)", esp_err_to_name(ret));
		return ESP_FAIL;
	}

	ESP_LOGD(TAG, "Partition size: total: %d, used: %d", total, used);
	return ESP_OK;
}

/**
 * @brief Init STA
 *        Initialize the WiFi STA interface
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_STA()
{
	ESP_LOGD(TAG, "Init STA mode");

	wifi_sta_config_t sta = {
		.ssid = CONFIG_WIFI_STA_SSID,
		.password = CONFIG_WIFI_STA_PASS
	};
	wifi_config.sta = sta;

	ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
	ESP_LOGD(TAG, "STA mode credentials: SSID:[%s] password:[%s]", sta.ssid, sta.password);

	ESP_ERROR_CHECK(esp_wifi_start());
	return tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, CONFIG_ENTITY);
}


/**
 * @brief Init WiFi
 *        Initialize the WiFi network adapter
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_wifi()
{
	ESP_LOGD(TAG, "Init NVS Flash");
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

	ESP_ERROR_CHECK(init_spiffs());

	ESP_LOGD(TAG, "Init Network Interface");

	netif_event_group = xEventGroupCreate();
	ESP_ERROR_CHECK(esp_netif_init());
	ESP_ERROR_CHECK(esp_event_loop_create_default());

    sta_netif = esp_netif_create_default_wifi_sta();
	assert(sta_netif);

	wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK(esp_wifi_init(&cfg));

	ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &network_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, ESP_EVENT_ANY_ID, &network_event_handler, NULL));

	ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));


	ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
	ESP_ERROR_CHECK(init_STA());
	xEventGroupWaitBits(netif_event_group, NETW_CONNECT_BIT | NETW_FAIL_BIT, pdFALSE, pdFALSE, portMAX_DELAY);

	ESP_ERROR_CHECK(server_start());

	return ESP_OK;
}
