set(COMPONENT_SRCDIRS "src")
set(COMPONENT_ADD_INCLUDEDIRS "include")

set(COMPONENT_REQUIRES log freertos driver nvs_flash esp_http_server mqtt spiffs branch data_center ec_station factory hospital house power_plant skyscraper solar_farm stadium substation supermarket test wind_farm)

register_component()
