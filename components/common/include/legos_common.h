//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_COMMON_H_
#define _LEGOS_COMMON_H_

#include <string.h>
#include <math.h>
#include <time.h>

// Timer settings
#define TIMER_DIVIDER 50000
#define TIMER_SCALE (TIMER_BASE_CLK / TIMER_DIVIDER)

// Message settings
#define	MSG_ENCODE_UL20 (0)
#define	MSG_ENCODE_JSON (1)
#define MSG_ENCODE MSG_ENCODE_UL20

/**
 * @brief Communication service topic
 */
static const char *mqtt_topic_service = "/legos/service/cmd";

/**
 * @brief Message payload structure
 */
typedef struct
{
	char key[64];
	char val[64];
	char str[128];
} msg_payload_t;

uint16_t msg_next_field(char* str, msg_payload_t* payload, uint16_t offset);
void msg_marshall(char* key, float value, msg_payload_t *payload);
int  msg_unmarshall(char* str, char* key, msg_payload_t *payload);


// LED intensity presets
typedef enum {
    LED_INTENSITY_0 = 0,
    LED_INTENSITY_10 = 819,
    LED_INTENSITY_20 = 1638,
    LED_INTENSITY_30 = 2457,
    LED_INTENSITY_40 = 3276,
    LED_INTENSITY_50 = 4096,
    LED_INTENSITY_60 = 4915,
    LED_INTENSITY_70 = 5734,
    LED_INTENSITY_80 = 6553,
    LED_INTENSITY_90 = 7372,
    LED_INTENSITY_100 = 8191
} led_duty_t;


#endif //_LEGOS_COMMON_H_