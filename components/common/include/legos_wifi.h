//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _LEGOS_WIFI_H_
#define _LEGOS_WIFI_H_


#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_spiffs.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "nvs_flash.h"

#include "legos_libs.h"
#include "legos_common.h"


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t init_wifi();

#ifdef __cplusplus
}
#endif //__cplusplus

#endif //_LEGOS_WIFI_H_