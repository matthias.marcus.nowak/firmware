//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

//  This library is partially based on:
//      infinityPV INA233 Arduino Library
//  Copyright:  2018, infinityPV ApS
//  Author:     rava (infinityPV ApS)
//  License:    BSD

#include "sdkconfig.h"
#include "esp_log.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/gpio.h"
#include "driver/i2c.h"

#include "ina233.h"
#include <string.h>
#include <math.h>

static const char *TAG = "ina233";

static i2c_port_t i2c_num = I2C_NUM_1;
static uint8_t i2c_address = 0;

//  Telemetry and Warning Conversion Coefficients (Datasheet: Table 1)
static pmbus_data_t pmbus_vb = {.m = 8, .R = 2, .b = 0};    // V_bus
static pmbus_data_t pmbus_vs = {.m = 4, .R = 5, .b = 0};    // V_shunt
static pmbus_data_t pmbus_is = {.m = 0, .R = 0, .b = 0};    // I_shunt
static pmbus_data_t pmbus_pw = {.m = 0, .R = 0, .b = 0};    // Power

/**
 * @brief  Write to I2C address
 *         Send N bytes from data to register reg
 * 
 * @param[in]  reg   Register address
 * @param[in]  data  Data buffer
 * @param[in]  N     Data size in bytes
 * 
 * @return void
 */
static void i2c_write(uint8_t reg, uint8_t *data, uint8_t N)
{
    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (i2c_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_DIS));
    ESP_ERROR_CHECK(i2c_master_write(cmd, &reg, 1, ACK_CHECK_DIS));
    for (i = 0; i < N; i++)
        ESP_ERROR_CHECK(i2c_master_write(cmd, &data[i], 1, ACK_CHECK_DIS));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);

    vTaskDelay(1 / portTICK_PERIOD_MS);
}


/**
 * @brief  Read from I2C address
 *         Read N bytes in data from register reg
 * 
 * @param[in]  reg   Register address
 * @param[out] data  Data buffer
 * @param[in]  N     Data size in bytes
 * 
 * @return void
 */
static void i2c_read(uint8_t reg, uint8_t *data, uint8_t size)
{
    // Send register to read from
    i2c_write(reg, NULL, 0);

    uint8_t i, block_size;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (i2c_address << 1) | I2C_MASTER_READ, ACK_CHECK_DIS));
    if (size>2)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &block_size, ACK_VAL));
    for (i = 0; i < size-1; i++)
        ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], ACK_VAL));
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], NACK_VAL));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);
}


/**
 * @brief  Get INA233 Raw Bus Voltage
 * 
 * @return value
 */
static int16_t ina233_getBusVoltage_raw()
{
    int16_t value = 0;
    i2c_read(READ_VIN, (uint8_t *)&value, 2);

    return value;
}


/**
 * @brief  Get INA233 Raw Shunt Voltage
 * 
 * @return value
 */
static int16_t ina233_getShuntVoltage_raw()
{
    int16_t value = 0;
    i2c_read(MFR_READ_VSHUNT, (uint8_t *)&value, 2);

    return value;
}


/**
 * @brief  Get INA233 Raw Current
 * 
 * @return value
 */
static int16_t ina233_getCurrent_raw()
{
    int16_t value = 0;
    i2c_read(READ_IIN, (uint8_t *)&value, 2);

    return value;
}


/**
 * @brief  Get INA233 Raw Power
 * 
 * @return value
 */
static int16_t ina233_getPower_raw()
{
    int16_t value = 0;
    i2c_read(READ_PIN, (uint8_t *)&value, 2);

    return value;
}


/**
 * @brief  Get INA233 Raw Energy
 * 
 * @return void
 */
static void ina233_getEnergy_raw(uint16_t *accumulator, uint8_t *roll_over, uint32_t *sample_count)
{
    uint8_t value[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    i2c_read(READ_EIN, value, 6);
    *accumulator = (value[1] << 8) | value[0];
    *roll_over = value[2];
    *sample_count = ((uint32_t)value[5] << 16);
    *sample_count = (((uint32_t)value[4] << 8) | *sample_count);
    *sample_count = ((uint32_t)value[3] | *sample_count);
}


/**
 * @brief  Get INA233 Bus Voltage (V)
 * 
 * @param[in] addr I2C device address
 * 
 * @return vbus
 */
float ina233_getBusVoltage_V(uint8_t addr)
{
    i2c_address = addr;
    uint16_t value = ina233_getBusVoltage_raw();
    float vbus = (value * pow(10, -pmbus_vb.R) - pmbus_vb.b) / pmbus_vb.m;

    return vbus;
}


/**
 * @brief  Get INA233 Shunt Voltage (mV)
 * 
 * @param[in] addr I2C device address
 * 
 * @return vshunt
 */
float ina233_getShuntVoltage_mV(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = ina233_getShuntVoltage_raw();
    float vshunt = (value * pow(10, -pmbus_vs.R) - pmbus_vs.b) / pmbus_vs.m;

    return vshunt * 1000.0;
}


/**
 * @brief  Get INA233 Current (mA)
 * 
 * @param[in] addr I2C device address
 * 
 * @return current
 */
float ina233_getCurrent_mA(uint8_t addr)
{
    i2c_address = addr;
    int16_t value = ina233_getCurrent_raw();
    float current = (value * pow(10, -pmbus_is.R) - pmbus_is.b) / pmbus_is.m;

    return current * 1000.0;
}


/**
 * @brief  Get INA233 Power (mW)
 * 
 * @param[in] addr I2C device address
 * 
 * @return power
 */
float ina233_getPower_mW(uint8_t addr)
{
    i2c_address = addr;
    uint16_t value = ina233_getPower_raw();
    float power = (value * pow(10, -pmbus_pw.R) - pmbus_pw.b) / pmbus_pw.m;

    return power * 1000.0;
}


/**
 * @brief  Get INA233 Average Power (mW)
 * 
 * @param[in] addr I2C device address
 * 
 * @return av_power
 */
float ina233_getAv_Power_mW(uint8_t addr)
{
    i2c_address = addr;
    uint16_t accumulator = 0;
    uint8_t roll_over = 0;
    uint32_t sample_count = 0;
    uint32_t accumulator_24 = 0;
    uint32_t raw_av_power = 0;
    float av_power = 0;
    ina233_getEnergy_raw(&accumulator, &roll_over, &sample_count);
    accumulator_24 = (uint32_t)roll_over * 65536 + (uint32_t)accumulator;
    raw_av_power = accumulator_24 / sample_count;
    av_power = (raw_av_power * pow(10, -pmbus_pw.R) - pmbus_pw.b) / pmbus_pw.m;

    return av_power * 1000.0;
}


/**
 * @brief  Set INA233 Calibration Registers
 *         Calculate PMBus DIRECT Data Format Coefficients
 * 
 * @param[in] addr  I2C device address
 * @param[in] R_sh  Shunt resistance
 * @param[in] I_max Max expected current
 * 
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t ina233_setCalibration(uint8_t addr, float R_sh, float I_max)
{
    i2c_address = addr;

    // Take absolute value
    R_sh = fabs(R_sh);
    I_max= fabs(I_max);

    // Determine LSB and calibration value
    double C_LSB = I_max / pow(2, 15);
    double P_LSB = 25.0 * C_LSB;
    uint32_t CAL = (uint16_t)(0.00512/(R_sh*C_LSB));

    // Check CAL is in the uint16 range
    if (CAL > 0xFFFF){
        ESP_LOGE(TAG, "Calibration out-of-range, increase R_sh or I_max");
        return ESP_FAIL;
    }
    
    // Write calibration value
    i2c_write(MFR_CALIBRATION, (uint8_t *)&CAL, 2);

    // Non-optimized coefficients
    double m_is = 1.0 / C_LSB;
    double m_pw = 1.0 / P_LSB;

    // Optimized coefficients
    pmbus_is.R = (int8_t)ceil(log10(m_is/32768));
    pmbus_pw.R = (int8_t)ceil(log10(m_pw/32768));

    pmbus_is.m = m_is * pow(10,-pmbus_is.R);
    pmbus_pw.m = m_pw * pow(10,-pmbus_pw.R);

    return ESP_OK;
}


/**
 * @brief  Get INA233 Manufacturer Info
 * 
 * @param[in] addr I2C device address
 * @param[out] model_ext Extended model
 * 
 * @return void
 */
void ina233_readInfo(uint8_t addr, char *model_ext)
{
    i2c_address = addr;
    uint8_t id[2];
    uint8_t model[2];
    uint8_t revision[2];

    i2c_read(TI_MFR_ID, id, 2);
    i2c_read(TI_MFR_MODEL, model, 2);
    i2c_read(TI_MFR_REVISION, revision, 2);
    ESP_LOGD(TAG, "Manufacturer: %c%c", id[1], id[0]);
    ESP_LOGD(TAG, "Model: INA2%c%c", model[1], model[0]);
    ESP_LOGD(TAG, "Revision: %c%c", revision[1], revision[0]);

    i2c_read(MFR_MODEL, (uint8_t *)model_ext, 6);
    ESP_LOGD(TAG, "Ext Model: %c%c%c%c%c%c", model_ext[0], model_ext[1], model_ext[2], model_ext[3], model_ext[4], model_ext[5]);
}


/**
 * @brief  Init INA233 power monitor
 * 
 * @param[in] v_i2c_num I2C port
 * @param[in] addr I2C device address
 * 
 * @return
 *     - ESP_OK   Success
 *     - ESP_FAIL Device init error
 */
esp_err_t ina233_init(i2c_port_t v_i2c_num, uint8_t addr)
{
    i2c_num = v_i2c_num;

    char model[7] = {'\0','\0','\0','\0','\0','\0','\0'};
    ina233_readInfo(addr, model);

    if (strcmp("INA233", model) != 0)
        return ESP_FAIL;
    else
        return ESP_OK;
}