# Branch
The branch is an active device for power distribution that allows to measure and control the power flowing among the entities. It controls the power flow by means of two bi-directional back-to-back switches. A third switch, connected between the center of the bus and ground, allows the emulation of grid faults. The flow in the two halves of the bus is monitored by two independent monitoring units, placed respectively upstream and downstream of the fault switch in proximity of the node interconnection. A magnetic Unipolar Hall effect switch IC with dual outputs allows for indipendent external triggering of the switches.

## Main code
### Identification
Replace **X** with branch number for easy device addressing via MQTT
```c
static const char *ID = "#X"        // Branch ID
```
### Status structure
```c
struct Branch
{
    uint8_t     id_a[8];            // Unique A port identifier
    uint8_t     id_b[8];            // Unique B port identifier
    fault_t     fault;              // Fault status
    switch_t    switchgear;         // Switchgear status
    float       led_intensity;      // Normalized global brightness
    telemetry_t bus_a;              // Bus A telemetry
    telemetry_t bus_b;              // Bus B telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> telemetry[Telemetry]
    telemetry --> power_flow[Spawn power flow thread]
    power_flow --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait[/wait Timer\]
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> status[Detect Faults]
    status --> flow[Update Power Flow]
    flow --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/grid**
    - Telemetry
        - *Format*: `<ID>   A: <Ia> mA [<Va> V]   B: <Ib> mA [<Vb> V]`
        - *Description*: Log telemetry data **ID**, **Ix** and **Vx** are the current and voltage measured at bus **x**<br/>(*eg.* `#10   A: 121.7 mA [3.36 V]   B: -121.1 mA [3.35 V]`)

### Subscribe
- **/legos/branch/cmd**
    - Open
        - Format: `open|<ID>`
        - Description: Open line switch
    - Close
        - Format: `close|<ID>`
        - Description: Close line switch
    - Fault
        - Format: `fault|<ID>`
        - Description: Enable fault switch
    - Clear
        - Format: `clear|<ID>`
        - Description: Clear fault switch
- **/legos/service/cmd**
    - Update
        - Format: `update|Branch`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `bus_a|<W>;bus_b|<W>`
        - *Description*: Log telemetry data, **W** is the power measured at bus A or B.(*eg.* `bus_a|0.32;bus_B|0.32`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Break
        - Format: `break|<val>`
        - Description: Set the line switch to **val** open [1] or closed [0]
    - Fault
        - Format: `fault|<val>`
        - Description: Set the fault switch to **val** open [0] or closed [1]

### WEB PAGE
[<img src="docs/web_page.png"  width="119" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/distribution/branch/branch.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/grid/branch/branch.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/grid/branch/branch.md)