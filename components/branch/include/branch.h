//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _BRANCH_H_
#define _BARNCH_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM_A    GPIO_NUM_5  // 1 Wire EEPROM
#define PIN_EEPROM_B    GPIO_NUM_15

#define PIN_TXD         GPIO_NUM_1  // Data communication
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_4  // I2C Pins
#define PIN_SCL         GPIO_NUM_0

#define PIN_LED_1A      GPIO_NUM_9  // LED Branch A
#define PIN_LED_2A      GPIO_NUM_10
#define PIN_LED_3A      GPIO_NUM_18
#define PIN_LED_4A      GPIO_NUM_23
#define PIN_LED_5A      GPIO_NUM_19
#define PIN_LED_6A      GPIO_NUM_22
#define PIN_LED_7A      GPIO_NUM_13

#define PIN_LED_1B      GPIO_NUM_12 // LED Branch B
#define PIN_LED_2B      GPIO_NUM_32
#define PIN_LED_3B      GPIO_NUM_33
#define PIN_LED_4B      GPIO_NUM_25
#define PIN_LED_5B      GPIO_NUM_26
#define PIN_LED_6B      GPIO_NUM_27
#define PIN_LED_7B      GPIO_NUM_14

#define PIN_BUS_BREAK   GPIO_NUM_21 // Bus control  enable (0) / disable (1)
#define PIN_BUS_FAULT   GPIO_NUM_2  // Bus fault    enable (1) / disable (0)

#define IRQ_INA_A       GPIO_NUM_35 // Interrupt Input of INA233
#define IRQ_INA_B       GPIO_NUM_34 // Interrupt Input of INA233

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum led_branch_a{
    LED_BRANCH_1A = 0,              // GPIO_NUM_9  Branch A LEDs
    LED_BRANCH_2A,                  // GPIO_NUM_10
    LED_BRANCH_3A,                  // GPIO_NUM_18
    LED_BRANCH_4A,                  // GPIO_NUM_23
    LED_BRANCH_5A,                  // GPIO_NUM_19
    LED_BRANCH_6A,                  // GPIO_NUM_22
    LED_BRANCH_7A,                  // GPIO_NUM_13
    LED_MAX_A,
};

enum led_branch_b{
    LED_BRANCH_1B = LED_MAX_A,      // GPIO_NUM_12 Branch B LEDs
    LED_BRANCH_2B,                  // GPIO_NUM_32
    LED_BRANCH_3B,                  // GPIO_NUM_33
    LED_BRANCH_4B,                  // GPIO_NUM_25
    LED_BRANCH_5B,                  // GPIO_NUM_26
    LED_BRANCH_6B,                  // GPIO_NUM_27
    LED_BRANCH_7B,                  // GPIO_NUM_14
    LED_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Switch status structure
 */
typedef struct{
    bool uv;                        // Under-voltage
    bool ov;                        // Over-voltage
    bool uc;                        // Under-current
    bool oc;                        // Over-current
    bool lc;                        // Leakage-current
} fault_t;

/**
 * @brief Switch status structure
 */
typedef struct{
    bool sc;                        // Parallel Short-circuit
    bool oc;                        // Series Open-circuit 
} switch_t;

/**
 * @brief Power-flow to LED speed mapping mode
 */
typedef enum{
    FLOW_LINEAR = 0,
    FLOW_QUADRATIC,
    FLOW_CUBIC,
} led_mapping_t;


/**
 * @brief Entity status structure
 */
struct Branch
{
    uint8_t     id_a[8];            // Unique A port identifier
    uint8_t     id_b[8];            // Unique B port identifier
    fault_t     fault;              // Fault status
    switch_t    switchgear;         // Switchgear status
    float       led_intensity;      // Normalized global brightness
    telemetry_t bus_a;              // Bus A telemetry
    telemetry_t bus_b;              // Bus B telemetry
} entity;


/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/grid";
static const char mqtt_topic_sub[] = "/legos/branch/cmd";

#ifdef __cplusplus
extern "C" {
#endif

esp_err_t branch_init_entity();
esp_err_t branch_http_resp(char *str);
esp_err_t branch_http_read(char *str);
esp_err_t branch_mqtt_init(void *client);
esp_err_t branch_mqtt_post(void *client);
esp_err_t branch_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() branch_init_entity()
#define http_resp(x) branch_http_resp(x)
#define http_read(x) branch_http_read(x)
#define mqtt_init(x) branch_mqtt_init(x)
#define mqtt_post(x) branch_mqtt_post(x)
#define mqtt_read(x) branch_mqtt_read(x)

#endif //_BRANCH_H_