//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "branch.h"
#include "esp_log.h"
#include "mqtt_client.h"

static const char *TAG = "Branch";                  // Logging tag
static const char *ID  = "#13";                      // Branch ID

static i2c_port_t i2c_num = I2C_NUM_1;              // I2C port
static const uint8_t ina_bus_a = INA233_ADDRESS_40; // I2C address INA233 
static const uint8_t ina_bus_b = INA233_ADDRESS_41;

static uint16_t pwrflow_timer_ms = 4;               // Power-flow Thread interval
static uint64_t pwrflow_timer_alarm;                // Power-flow Thread timer
static TaskHandle_t pwrflow_task_handle;            // Power-flow Thread task handle

static uint16_t entity_timer_ms = 100;              // Main-tasks Thread interval
static uint64_t entity_timer_alarm;                 // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;             // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN,       // Timer configuration
                              TIMER_PAUSE,
                              TIMER_INTR_LEVEL,
                              TIMER_COUNT_UP,
                              TIMER_AUTORELOAD_EN,
                              TIMER_DIVIDER};

static esp_mqtt_client_handle_t mqtt_client;        // MQTT client handle
static esp_mqtt_event_handle_t mqtt_event;          // MQTT event handle

static float led_intensity;                         // LED intensity absolute
static float Imax;                                  // Bus current max
static float In_a, In_b;                            // Bus current normalized
static int16_t Id_a, Id_b;                          // Bus current discretized
static uint8_t led_max_a, led_max_b;                // Bus LED update counter 
static uint8_t remote_override = 0;

// LEDC channel configuration
static ledc_channel_config_t pwm_channel[LED_MAX] =
{
	{
        .channel = 0,
        .duty = 0,
        .gpio_num = PIN_LED_1A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LED_2A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LED_3A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 3,
        .duty = 0,
        .gpio_num = PIN_LED_4A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 4,
        .duty = 0,
        .gpio_num = PIN_LED_5A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 5,
        .duty = 0,
        .gpio_num = PIN_LED_6A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 6,
        .duty = 0,
        .gpio_num = PIN_LED_7A,
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 0,
        .duty = 0,
        .gpio_num = PIN_LED_1B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
	{
        .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LED_2B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
	{
        .channel = 2,
        .duty = 0,
        .gpio_num = PIN_LED_3B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
	{
        .channel = 3,
        .duty = 0,
        .gpio_num = PIN_LED_4B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
	{
        .channel = 4,
        .duty = 0,
        .gpio_num = PIN_LED_5B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
	{
        .channel = 5,
        .duty = 0,
        .gpio_num = PIN_LED_6B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
	{
        .channel = 6,
        .duty = 0,
        .gpio_num = PIN_LED_7B,
        .speed_mode = PWM_HS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_HS_TIMER
    },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer =
    {
        .duty_resolution = LEDC_TIMER_13_BIT,   // PWM duty-cycle resolution
        .freq_hz = 5000,                        // PWM frequency
        .speed_mode = PWM_LS_MODE,              // PWM timer mode
        .timer_num = PWM_LS_TIMER,              // PWM timer index
        .clk_cfg = LEDC_AUTO_CLK,               // Auto select the source clock
};

//
//------------------------------------------------------------SERVICE FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 * 
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty, fade_time_ms);
    ledc_fade_start(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG,"PWM fade\tch:%d@%d in %d ms",ch,duty,fade_time_ms);
}


/**
 * @brief Set PWM channel dutycycle
 * 
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * 
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG,"PWM switch\tch:%d@%d",ch,duty);
}


/**
 * @brief Update LED power flow coefficients
 * 
 * @param[in] mode LED mapping mode (Linear, Quadratic, Cubic)
 * 
 * @return void
 */
void led_update_flow(led_mapping_t mode)
{
    In_a = (fabs(entity.bus_a.current) < Imax) ?
         (entity.bus_a.current / Imax) :
         (entity.bus_a.current / fabs(entity.bus_a.current));
    In_b = (fabs(entity.bus_b.current) < Imax) ?
         (entity.bus_b.current / Imax) :
         (entity.bus_b.current / fabs(entity.bus_b.current));

    Id_a = In_a * 255.0;
    Id_b = In_b * 255.0;

    switch (mode)
    {
    case FLOW_LINEAR:{
        led_max_a = (Id_a > 0) ? (259 - Id_a) : (259 + Id_a);
        led_max_b = (Id_b > 0) ? (259 - Id_b) : (259 + Id_b);
    }break;

    case FLOW_QUADRATIC:{
        led_max_a = (Id_a > 0) ? ((In_a-1)*(In_a-1)*251+4) : ((In_a+1)*(In_a+1)*251+4);
        led_max_b = (Id_b > 0) ? ((In_b-1)*(In_b-1)*251+4) : ((In_b+1)*(In_b+1)*251+4);
    }break;

    case FLOW_CUBIC:{
        led_max_a = (Id_a > 0) ? ((1-In_a)*(1-In_a)*(1-In_a)*251+4) : ((In_a+1)*(In_a+1)*(In_a+1)*251+4);
        led_max_b = (Id_b > 0) ? ((1-In_b)*(1-In_b)*(1-In_b)*251+4) : ((In_b+1)*(In_b+1)*(In_b+1)*251+4);
    }break;

    default:
        break;
    }

}


/**
 * @brief Update LED power flow task
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void led_pwrflow_task(void *arg)
{   
    uint8_t led_count_a = 0;
    uint8_t led_count_b = 0;
    float   led_seq_null [7] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    float   led_seq_pos_a[7] = {0.0,0.0,0.0,156E-4,625E-4,25E-2,1.0};
    float   led_seq_pos_b[7] = {0.0,0.0,0.0,156E-4,625E-4,25E-2,1.0};
    float   led_seq_neg_a[7] = {1.0,25E-2,625E-4,156E-4,0.0,0.0,0.0};
    float   led_seq_neg_b[7] = {1.0,25E-2,625E-4,156E-4,0.0,0.0,0.0};
    float  *led_seq_a, *led_seq_b;

    while(1) {
        xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);
        
        // Check power flow direction
        if (Id_a == 0)
            led_seq_a = led_seq_null;
        else if (Id_a < 0)
            led_seq_a = led_seq_pos_a;
        else
            led_seq_a = led_seq_neg_a;

        if (Id_b == 0)
            led_seq_b = led_seq_null;
        else if (Id_b > 0)
            led_seq_b = led_seq_pos_b;
        else
            led_seq_b = led_seq_neg_b;

        // Show power flow bus A
        if (++led_count_a >= led_max_a) {
            led_count_a = 0;
            
            // Rotate LED intensity sequence
            if (Id_a > 0){
                float temp = led_seq_a[0];
                led_seq_a[0] = led_seq_a[1];
                led_seq_a[1] = led_seq_a[2];
                led_seq_a[2] = led_seq_a[3];
                led_seq_a[3] = led_seq_a[4];
                led_seq_a[4] = led_seq_a[5];
                led_seq_a[5] = led_seq_a[6];
                led_seq_a[6] = temp;
            } else if (Id_a < 0){
                float temp = led_seq_a[6];
                led_seq_a[6] = led_seq_a[5];
                led_seq_a[5] = led_seq_a[4];
                led_seq_a[4] = led_seq_a[3];
                led_seq_a[3] = led_seq_a[2];
                led_seq_a[2] = led_seq_a[1];
                led_seq_a[1] = led_seq_a[0];
                led_seq_a[0] = temp;
            }

            for (int ch=0; ch<LED_MAX_A; ch++)
                pwm_switch(ch, led_seq_a[ch]*led_intensity);
        }

        // Show power flow bus B
        if (++led_count_b >= led_max_b) {
            led_count_b = 0;
            
            // Rotate LED intensity sequence
            if (Id_b > 0){
                float temp = led_seq_b[6];
                led_seq_b[6] = led_seq_b[5];
                led_seq_b[5] = led_seq_b[4];
                led_seq_b[4] = led_seq_b[3];
                led_seq_b[3] = led_seq_b[2];
                led_seq_b[2] = led_seq_b[1];
                led_seq_b[1] = led_seq_b[0];
                led_seq_b[0] = temp;
            } else if (Id_b < 0){
                float temp = led_seq_b[0];
                led_seq_b[0] = led_seq_b[1];
                led_seq_b[1] = led_seq_b[2];
                led_seq_b[2] = led_seq_b[3];
                led_seq_b[3] = led_seq_b[4];
                led_seq_b[4] = led_seq_b[5];
                led_seq_b[5] = led_seq_b[6];
                led_seq_b[6] = temp;
            }

            for (int ch=LED_MAX_A; ch<LED_MAX; ch++)
                pwm_switch(ch, led_seq_b[ch-LED_MAX_A]*led_intensity);
        }
    }
}


/**
 * @brief Read INA233 Telemetry
 * 
 * @param[in] addr Addres of the device on the I2C bus
 * 
 * @return void
 */
void read_telemetry(uint8_t addr)
{
    switch (addr)
    {
        case ina_bus_a:
        {
            entity.bus_a.voltage = ina233_getBusVoltage_V(addr);
            entity.bus_a.current = ina233_getCurrent_mA(addr);
            entity.bus_a.power = ina233_getPower_mW(addr);
            ESP_LOGD(TAG,"Telemetry\tbus A: %f V,shunt: %f mA, pwr: %f mW",entity.bus_a.voltage,entity.bus_a.current,entity.bus_a.power);
            break;
        }
        case ina_bus_b:
        {
            entity.bus_b.voltage = ina233_getBusVoltage_V(addr);
            entity.bus_b.current = ina233_getCurrent_mA(addr);
            entity.bus_b.power = ina233_getPower_mW(addr);
            ESP_LOGD(TAG,"Telemetry \tbus B: %f V,shunt: %f mA, pwr: %f mW",entity.bus_b.voltage,entity.bus_b.current,entity.bus_b.power);
            break;
        }
    }    
}


/**
 * @brief Detect fault status 
 * 
 * @return void
 */
void detect_fault_status()
{
    // Check over-voltage
    if ((entity.bus_a.voltage > 3.6) || (entity.bus_b.voltage > 3.6))
        entity.fault.ov = true;
    else
        entity.fault.ov = false;

    // Check under-voltage
    if ((entity.bus_a.voltage < 3.0) || (entity.bus_b.voltage < 3.0))
        entity.fault.uv = true;
    else
        entity.fault.uv = false;

    // Check over-current
    if ((fabs(entity.bus_a.current) > 4000.0) || (fabs(entity.bus_b.current) > 4000.0))
        entity.fault.oc = true;
    else
        entity.fault.oc = false;

    // Check under-current
    if ((fabs(entity.bus_a.current) < 2.5) || (fabs(entity.bus_b.current) < 2.5))
        entity.fault.uc = true;
    else
        entity.fault.uc = false;

    // Check leakage-current
    double delta = fabs(entity.bus_a.current + entity.bus_b.current);
    if (delta > 5)
        entity.fault.lc = true;
    else
        entity.fault.lc = false;
}


/**
 * @brief Entity main task timer ISR
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
	// Clear interrupt
	TIMERG0.int_clr_timers.t0 = 1;

	// Restart timer
	timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

	// Notify task
	xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
	portYIELD_FROM_ISR();
}


/**
 * @brief LED power flow task timer ISR
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void IRAM_ATTR pwrflow_timer_isr(void* arg)
{
	// Clear interrupt
	TIMERG0.int_clr_timers.t1 = 1;

	// Restart timer
	timer_set_alarm(TIMER_GROUP_0, TIMER_1, TIMER_ALARM_EN);

	// Notify task
	xTaskNotifyFromISR(pwrflow_task_handle, 0x00, eNoAction, NULL);
	portYIELD_FROM_ISR();
}


/**
 * @brief Entity main task
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void entity_main_tasks(void* arg)
{
    Imax = 3500.0;     // LED saturation current [mA]

	while (1)
	{
		// Wait timer
		xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

        // Get data
		read_telemetry(ina_bus_a);
        read_telemetry(ina_bus_b);

        // Get absolute LED intensity
        led_intensity = entity.led_intensity*LED_INTENSITY_100;

        // Check fault status
        detect_fault_status();

        // Update LED power flow speed
        led_update_flow(FLOW_QUADRATIC);

        // Update switches
        if(remote_override){
            if ((remote_override-1)==0){
                gpio_set_level(PIN_BUS_BREAK, 0);
                gpio_set_level(PIN_BUS_FAULT, 0);
            }else{
                remote_override--;
            }
        }
    }
	vTaskDelete(NULL);
}

//
//------------------------------------------------------------INIT FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Init GPIO
 * 
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = ((1ULL << PIN_BUS_BREAK) |
                            (1ULL << PIN_BUS_FAULT));
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);

    gpio_set_level(PIN_BUS_BREAK, 0);
    gpio_set_level(PIN_BUS_FAULT, 0);

    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pin_bit_mask = ((1ULL << IRQ_INA_A) |
                            (1ULL << IRQ_INA_B));
    gpio_config(&io_conf);

    return ESP_OK;
}


/**
 * @brief Init PWM channels
 * 
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer0 for low speed channels
    // Prepare and set configuration of timer1 for high speed channels
    pwm_timer.speed_mode = PWM_HS_MODE;
    pwm_timer.timer_num = PWM_HS_TIMER;
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer1 for high speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0));                                     // Init fade service
}


/**
 * @brief Init 1-Wire interface
 * 
 *        Initialize the 1-Wire interface and detect the 64-bit ID eeprom connected to
 *        the bus A and B
 * @note  Return ESP_FAIL in case of branch not terminated on a node and trigger a reset
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t init_owb()
{
    // esp_log_level_set("owb", ESP_LOG_INFO);
    // esp_log_level_set("owb_rmt", ESP_LOG_INFO);

    // Create a 1-Wire bus, using the RMT timeslot driver
    OneWireBus *owb;
    owb_rmt_driver_info rmt_driver_info;
    owb = owb_rmt_initialize(&rmt_driver_info, PIN_EEPROM_A, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true); // enable CRC check for ROM code
    owb_use_parasitic_power(owb, true);

    // Find connected device
    OneWireBus_SearchState search_state = {0};
    bool found = false;
    owb_search_first(owb, &search_state, &found);
    OneWireBus_ROMCode device_rom_code = search_state.rom_code;
    
    ESP_LOGD(TAG, "Node ID @ bus #A:");
    if (found)
    {
        esp_log_buffer_hex_internal(TAG, device_rom_code.bytes, 8, ESP_LOG_DEBUG);
        memcpy(entity.id_a, device_rom_code.bytes, 8);
        // return ESP_OK;
    }
    else
    {
        ESP_LOGD(TAG, "not valid");
        return ESP_FAIL;
    }
    owb_uninitialize(owb);

    // Create a 1-Wire bus, using the RMT timeslot driver
    owb = owb_rmt_initialize(&rmt_driver_info, PIN_EEPROM_B, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true); // enable CRC check for ROM code
    owb_use_parasitic_power(owb, true);

    // Find connected device
    memset(&search_state, 0, sizeof(OneWireBus_SearchState));
    found = false;
    owb_search_first(owb, &search_state, &found);
    device_rom_code = search_state.rom_code;
    
    ESP_LOGD(TAG, "Node ID @ bus #B:");
    if (found)
    {
        esp_log_buffer_hex_internal(TAG, device_rom_code.bytes, 8, ESP_LOG_DEBUG);
        memcpy(entity.id_b, device_rom_code.bytes, 8);
        owb_uninitialize(owb);
        return ESP_OK;
    }
    else
    {
        ESP_LOGD(TAG, "not valid");
        return ESP_FAIL;
    }
}


/**
 * @brief Init the I2C interface
 * 
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
esp_err_t init_i2c()
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    ESP_LOGD(TAG, "sda_io_num %d", PIN_SDA);

    conf.sda_io_num = PIN_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "scl_io_num %d", PIN_SCL);

    conf.scl_io_num = PIN_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "clk_speed %d", I2C_MASTER_FREQ_HZ);

    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    ESP_LOGD(TAG, "i2c_param_config %d", conf.mode);
    ESP_ERROR_CHECK(i2c_param_config(i2c_num, &conf));

    ESP_LOGD(TAG, "i2c_driver_install %d", i2c_num);
    return (i2c_driver_install(i2c_num, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0));
}


/**
 * @brief Init the INA233 power monitor
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_telemetry()
{
    /*
    VBus Rail
    | Part        | Count     | Max     |
    | ----------- | :-------: | ------: |
    | PowerPlant  |  1        |   3.6 A |
    | SolarFarm   |  1        |   1.2 A |
    | WindFarm    |  1        |   1.2 A |
    | House       |  1        |  0.25 A |
    | Supermarket |  1        |  0.25 A |
    | Total       |           |   6.5 A |
    */
  
    float INA_R_SHUNT = 1E-2F;
    float INA_I_MAX = 6.5;

    ESP_ERROR_CHECK(ina233_init(i2c_num, ina_bus_a));
    ESP_ERROR_CHECK(ina233_init(i2c_num, ina_bus_b));
    ESP_ERROR_CHECK(ina233_setCalibration(ina_bus_a, INA_R_SHUNT, INA_I_MAX));
    ESP_ERROR_CHECK(ina233_setCalibration(ina_bus_b, INA_R_SHUNT, INA_I_MAX));

    return ESP_OK;
}

/**
 * @brief Init timer for LED power flow
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_pwrflow_timer()
{
    pwrflow_timer_alarm = (uint64_t) pwrflow_timer_ms * TIMER_SCALE / 1000ULL;
    
	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_1, &cfg));
	ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_1));
	ESP_ERROR_CHECK(timer_set_counter_value(TIMER_GROUP_0, TIMER_1, 0x00000000ULL));
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_1, pwrflow_timer_alarm));
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_1));
	ESP_ERROR_CHECK(timer_isr_register(TIMER_GROUP_0, TIMER_1, pwrflow_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_1));

    ESP_LOGD(TAG, "Power flow interval set to: %d ms", pwrflow_timer_ms);
    
    return ESP_OK;
}


/**
 * @brief Init timer for main entity task
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
    Id_a = 0;
    Id_b = 0;

	entity_timer_alarm = (uint64_t) entity_timer_ms * TIMER_SCALE / 1000ULL;

	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
	ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_isr_register(TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));

	ESP_LOGD(TAG, "Entity interval set to: %d ms", entity_timer_ms);

	return ESP_OK;
}


/**
 * @brief Init entity
 *        Initialize the entity-specific submodules and start the activity tasks
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t branch_init_entity()
{
    // Set default LED intensity
    entity.led_intensity = 0.5;

    init_gpio();
    init_pwm();
    init_owb();
    init_i2c();
    init_telemetry();

    // Power flow visualization task
    xTaskCreate(led_pwrflow_task, "led_pwrflow_task", 2048, NULL, 5, &pwrflow_task_handle);
    ESP_ERROR_CHECK(init_pwrflow_timer());

    // Entity main tasks
	xTaskCreate(entity_main_tasks, "entity_main_task", 2048, NULL, 4, &entity_task_handle);
	ESP_ERROR_CHECK(init_tasks_timer());

    return ESP_OK;
}

//
//------------------------------------------------------------EXTERNAL CALLBACKS-------------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t branch_http_resp(char* str)
{
    sprintf(str, "bus_a|%.2f;bus_b|%.2f",
            entity.bus_a.power/1000,
            entity.bus_b.power/1000);

	return ESP_OK;
}



/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t branch_http_read(char *str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
    }

    if (msg_unmarshall(str, "break", &msg_payload)){
            uint8_t val = atoi(msg_payload.val);
            if ((val>=0) && (val<=1)){
                remote_override = 255;          // 25 s
                gpio_set_level(PIN_BUS_BREAK, val);
                ESP_LOGD(TAG, "LINE SWITCH");
            }
            return ESP_OK;
    }

    if (msg_unmarshall(str, "fault", &msg_payload)){
            uint8_t val = atoi(msg_payload.val);
            if ((val>=0) && (val<=1)){
                remote_override = 150;          // 15 s
                gpio_set_level(PIN_BUS_FAULT, val);
                ESP_LOGD(TAG, "FAULT SWITCH");
            }
            return ESP_OK;
    }

    return ESP_FAIL;
}


/**
 * @brief MQTT init topics
 *        Subscribe to entity-specific topics
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t branch_mqtt_init(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_service, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_service);

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_sub, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_sub);

    return ESP_OK;
}


/**
 * @brief MQTT post
 *        Callback for periodic publishing updates via MQTT
 * 
 * @param[in] client Pointer to MQTT client handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t branch_mqtt_post(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;
    char data[64];
    sprintf(data,"%3s A: %7.1f mA [%4.2f V]\tB: %7.1f mA [%4.2f V]",ID,entity.bus_a.current,entity.bus_a.voltage,entity.bus_b.current,entity.bus_b.voltage);
    esp_mqtt_client_publish(mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
    return ESP_OK;
}


/**
 * @brief MQTT read
 *        Callback for reading subscribed topics via MQTT
 * 
 * @param[in] event Pointer to MQTT event handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t branch_mqtt_read(void *event)
{
    mqtt_event = (esp_mqtt_event_handle_t)event;

    char* data = (char*)malloc(mqtt_event->data_len+1);
    memcpy(data,mqtt_event->data,mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len+1);
    memcpy(topic,mqtt_event->topic,mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic,mqtt_topic_service)){
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val,TAG)){
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return ESP_OK;
            }

        if (msg_unmarshall(data, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
        }
    }

    if (!strcmp(topic,mqtt_topic_sub)){
        if (msg_unmarshall(data, "open", &msg_payload))
            if (!strcmp(msg_payload.val,ID)){
                ESP_LOGD(TAG, "OPEN");
                gpio_set_level(PIN_BUS_BREAK, 1);
                return ESP_OK;
            }

        if (msg_unmarshall(data, "close", &msg_payload))
            if (!strcmp(msg_payload.val,ID)){
                ESP_LOGD(TAG, "CLOSE");
                gpio_set_level(PIN_BUS_BREAK, 0);
                return ESP_OK;
            }

        if (msg_unmarshall(data, "fault", &msg_payload))
            if (!strcmp(msg_payload.val,ID)){
                ESP_LOGD(TAG, "FAULT");
                gpio_set_level(PIN_BUS_FAULT, 1);
                return ESP_OK;
            }

        if (msg_unmarshall(data, "clear", &msg_payload))
            if (!strcmp(msg_payload.val,ID)){
                ESP_LOGD(TAG, "CLEAR");
                gpio_set_level(PIN_BUS_FAULT, 0);
                return ESP_OK;
            }
    }
    return ESP_OK;
}