# 1-Wire
## Description
This component provides an ESP32 library for interfacing devices via 1-Wire communication bus system designed by Dallas Semiconductor Corp. The library was developed by David Antliff and Chris Morgan and released with MIT license under the name esp32-owb.

## Main code
### Methods
```c
// Init OWB using RMT module
OneWireBus* owb_rmt_initialize(owb_rmt_driver_info * info, gpio_num_t gpio_num,
                                rmt_channel_t tx_channel, rmt_channel_t rx_channel)
```

## Documents
Original Library: [esp32-owb](https://github.com/DavidAntliff/esp32-owb)
