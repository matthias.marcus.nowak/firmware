//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _EC_STATION_H_
#define _EC_STATION_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/dac.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "MFRC522.h"
#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------

#define PIN_EEPROM      GPIO_NUM_0  // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1  // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_19 // I2C Pins
#define PIN_SCL         GPIO_NUM_21

#define PIN_MISO        GPIO_NUM_26 // SPI Pins
#define PIN_MOSI        GPIO_NUM_27
#define PIN_CLK         GPIO_NUM_14
#define PIN_CS1         GPIO_NUM_13
#define PIN_CS2         GPIO_NUM_5
#define PIN_RST         GPIO_NUM_15

#define PIN_CAR1_R      GPIO_NUM_33 // LEDs car charging
#define PIN_CAR1_G      GPIO_NUM_32
#define PIN_CAR2_R      GPIO_NUM_17
#define PIN_CAR2_G      GPIO_NUM_16
#define PIN_LOAD        GPIO_NUM_25 // Virtual Load (DAC 1)

#define IRQ_INA         GPIO_NUM_18 // INA233 Interrupt Input
#define IRQ_Car1        GPIO_NUM_35 // RFID 1 Interrupt Input
#define IRQ_Car2        GPIO_NUM_4  // RFID 2 Interrupt Input

// ---------- PWM channel Definitions ----------

#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum led_car{
    LED_CAR1_R = 0,     // GPIO_NUM_33
    LED_CAR1_G,         // GPIO_NUM_32
    LED_CAR2_R,         // GPIO_NUM_17
    LED_CAR2_G,         // GPIO_NUM_16
    LED_CAR_MAX
};

// ---------- Entity Definitions ----------

/**
 * @brief Slot occupation
 */
typedef enum 
{
    SLOT_NONE = 0,
    SLOT_RIGHT,
    SLOT_LEFT,
    SLOT_BOTH,
} slot_t;

/**
 * @brief RFID tag status
 */
typedef enum
{    
    TAG_GONE = 0,
    TAG_PRESENT
} tag_status_t;

/**
 * @brief RFID tag flag
 */
typedef enum
{
    READ_TAG = 0,
    NO_READ_TAG
} tag_flag_t;

/**
 * @brief RFID status structure
 */
typedef struct      
{
    bool status;                    // Tag already read
    bool detect;                    // Tag detect
    bool load;                      // load info of current step
    uint8_t error_counter;          // Tag presence counteer
    uint8_t storage[18];            // Tag block buffer
    uint8_t BlockAddr;              // Tag block address
    int8_t animation_count;         // Animation index of storage
    int8_t animation_fail;          // Fault visualization tag
} rfid_info_t;

/**
 * @brief Entity status structure
 */
struct ECStation
 {
    uint8_t     id[8];              // Unique port identifier
    uint8_t     charging_load;      // Charging load
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/ec_station";
static const char mqtt_topic_sub[] = "/legos/ec_station/cmd";


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t ecstation_init_entity();
esp_err_t ecstation_http_resp(char *str);
esp_err_t ecstation_http_read(char *str);
esp_err_t ecstation_mqtt_init(void *client);
esp_err_t ecstation_mqtt_post(void *client);
esp_err_t ecstation_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() ecstation_init_entity()
#define http_resp(x) ecstation_http_resp(x)
#define http_read(x) ecstation_http_read(x)
#define mqtt_init(x) ecstation_mqtt_init(x)
#define mqtt_post(x) ecstation_mqtt_post(x)
#define mqtt_read(x) ecstation_mqtt_read(x)

#endif //_EC_STATION_H_