# Hospital
The Hospital represents one of the critical infrastructure for health service, which must provide uninterrupted operation even during a power outage. The workload is defined by the emergency status, which is triggered by the presence of an emergency vehicle on the roof of the building or at the entrance, detected using a magnetic field sensor.

## Main code
### Status structure
```c
struct Hospital
{
    uint8_t     id[8];              // Unique port identifier
    bool        power_source;       // Power source (0 GRID, 1 UPS)
    float       led_intensity;      // Normalized global brightness
    emergency_t status;             // Emergency sensors status
    telemetry_t power_in;           // Input telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> telemetry[Telemetry]
    telemetry --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> status[Detect Emergency]
    status --> source[Detect Source]
    source --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/hospital**
    - Telemetry
        - *Format*: `Hospital: <I> mA [<V> V]   Source: <val>`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter. Source **val** can be either *GRID* or *UPS*<br/>(*eg.* `Hospital: 121.7 mA [3.36 V]   Source: UPS`)

### Subscribe
- **/legos/hospital/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|Hospital`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>;source|<val>`
        - *Description*: Log telemetry data, **W** is the input power measured at the meter. Source **val** can be either *grid* or *ups*<br/>(*eg.* `power|0.32;source|grid`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Emergency
        - Format: `status|<val>`
        - Description: Set emergency status to **val**: none (0), ambulance (1), helicopter (2), both(3)
### WEB PAGE
[<img src="docs/web_page.png"  width="120" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/hospital/hospital.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/hospital/hospital.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/hospital/hospital.md)