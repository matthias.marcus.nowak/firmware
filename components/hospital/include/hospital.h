//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _HOSPITAL_H_
#define _HOSPITAL_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"


// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0  // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1  // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_33 // I2C Pins
#define PIN_SCL         GPIO_NUM_25

#define PIN_SRC         GPIO_NUM_15 // Power source selection (0 GRID, 1 UPS)

#define PIN_SENS_HELI   GPIO_NUM_17 // Magnetic switches
#define PIN_SENS_CAR    GPIO_NUM_5 
#define PIN_LED_R0      GPIO_NUM_4  // LED rooms
#define PIN_LED_R1      GPIO_NUM_16 

#define PIN_UPS_1       GPIO_NUM_2  // LED UPS
#define PIN_UPS_2       GPIO_NUM_13
#define PIN_UPS_3       GPIO_NUM_12

#define IRQ_INA         GPIO_NUM_32 // INA233 Interrupt

// ---------- PWM channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum led_emergency{
    LED_ROOM_0 = 0,                 // GPIO_NUM_4 Room lights
    LED_ROOM_1,
    LED_ROOM_MAX,
};

enum led_ups{
    LED_UPS_1 = LED_ROOM_MAX,       // GPIO_NUM_2  UPS LEDs
    LED_UPS_2,                      // GPIO_NUM_13
    LED_UPS_3,                      // GPIO_NUM_12
    LED_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Alert level
 */
typedef enum 
{
    EMERGENCY_NONE = 0,
    EMERGENCY_CAR,
    EMERGENCY_HELI,
    EMERGENCY_BOTH,
} emergency_t;
  
/**
 * @brief Entity status structure
 */
struct Hospital
{
    uint8_t     id[8];              // Unique port identifier
    bool        power_source;       // Power source (0 GRID, 1 UPS)
    float       led_intensity;      // Normalized global brightness
    emergency_t status;             // Emergency sensors status
    telemetry_t power_in;           // Input telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/hospital";
static const char mqtt_topic_sub[] = "/legos/hospital/cmd";


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t hospital_init_entity();
esp_err_t hospital_http_resp(char *str);
esp_err_t hospital_http_read(char *str);
esp_err_t hospital_mqtt_init(void *client);
esp_err_t hospital_mqtt_post(void *client);
esp_err_t hospital_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() hospital_init_entity()
#define http_resp(x) hospital_http_resp(x)
#define http_read(x) hospital_http_read(x)
#define mqtt_init(x) hospital_mqtt_init(x)
#define mqtt_post(x) hospital_mqtt_post(x)
#define mqtt_read(x) hospital_mqtt_read(x)

#endif