//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "hospital.h"
#include "esp_log.h"
#include "mqtt_client.h"

static const char *TAG = "Hospital";                // Logging tag

static i2c_port_t i2c_num = I2C_NUM_1;              // I2C port
static uint8_t ina_grid = INA233_ADDRESS_40;        // I2C address INA233

static uint16_t entity_timer_ms = 100;              // Main-tasks Thread interval
static uint64_t entity_timer_alarm;                 // Main-tasks Thread timer
static TaskHandle_t entity_task_handle;             // Main-tasks Thread task handle
static timer_config_t cfg = { TIMER_ALARM_EN,       // Timer configuration
                              TIMER_PAUSE,
                              TIMER_INTR_LEVEL,
                              TIMER_COUNT_UP,
                              TIMER_AUTORELOAD_EN,
                              TIMER_DIVIDER};

static esp_mqtt_client_handle_t mqtt_client;        // MQTT client handle
static esp_mqtt_event_handle_t mqtt_event;          // MQTT event handle
static emergency_t status_remote = EMERGENCY_NONE;

// LEDC channels configuration
static ledc_channel_config_t pwm_channel[LED_MAX] =
{
	{
        .channel = 0,
        .duty = 0,
        .gpio_num = PIN_LED_R0,        // LED_ROOM_0
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 1,
        .duty = 0,
        .gpio_num = PIN_LED_R1,        // LED_ROOM_1
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 2,
        .duty = 0,
        .gpio_num = PIN_UPS_1,        // LED_UPS_1
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 3,
        .duty = 0,
        .gpio_num = PIN_UPS_2,        // LED_UPS_2
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
	{
        .channel = 4,
        .duty = 0,
        .gpio_num = PIN_UPS_3,        // LED_UPS_3
        .speed_mode = PWM_LS_MODE,
        .hpoint = 0,
        .timer_sel = PWM_LS_TIMER
    },
};

// LEDC timer configuration
static ledc_timer_config_t pwm_timer =
    {
        .duty_resolution = LEDC_TIMER_13_BIT,   // PWM duty-cycle resolution
        .freq_hz = 5000,                        // PWM frequency
        .speed_mode = PWM_LS_MODE,              // PWM timer mode
        .timer_num = PWM_LS_TIMER,              // PWM timer index
        .clk_cfg = LEDC_AUTO_CLK,               // Auto select the source clock
};

//
//------------------------------------------------------------SERVICE FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Fade PWM channel dutycycle in a given time
 *
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * @param[in] fade_time_ms Max fading time in ms
 * 
 * @return void
 */
void pwm_fade(uint8_t ch, uint32_t duty, int fade_time_ms)
{
    ledc_set_fade_with_time(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty, fade_time_ms);
    ledc_fade_start(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, LEDC_FADE_NO_WAIT);
    ESP_LOGD(TAG,"PWM fade\tch:%d@%d in %d ms",ch,duty,fade_time_ms);
}


/**
 * @brief Set PWM channel dutycycle
 * 
 * @param[in] ch PWM channel number
 * @param[in] duty PWM duty cycle
 * 
 * @return void
 */
void pwm_switch(uint8_t ch, uint32_t duty)
{
    ledc_set_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel, duty);
    ledc_update_duty(pwm_channel[ch].speed_mode, pwm_channel[ch].channel);
    ESP_LOGD(TAG,"PWM switch\tch:%d@%d",ch,duty);
}


/**
 * @brief Read INA233 Telemetry
 * 
 * @param[in] addr Addres of the device on the I2C bus
 * 
 * @return void
 */
void read_telemetry(uint8_t addr)
{
    entity.power_in.voltage = ina233_getBusVoltage_V(addr);
    entity.power_in.current = ina233_getCurrent_mA(addr);
    entity.power_in.power = ina233_getPower_mW(addr);
    ESP_LOGD(TAG,"Telemetry\tbus: %f V,shunt: %f mA, pwr: %f mW",entity.power_in.voltage,entity.power_in.current,entity.power_in.power);
}


/**
 * @brief Entity main task timer ISR
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void IRAM_ATTR entity_timer_isr(void* arg)
{
	// Clear interrupt
	TIMERG0.int_clr_timers.t0 = 1;

	// Restart timer
	timer_set_alarm(TIMER_GROUP_0, TIMER_0, TIMER_ALARM_EN);

	// Notify task
	xTaskNotifyFromISR(entity_task_handle, 0x00, eNoAction, NULL);
	portYIELD_FROM_ISR();
}


/**
 * @brief Entity main task
 * 
 * @param[in] arg Parameters passed to the task
 * 
 * @return void
 */
void entity_main_tasks(void* arg)
{
    // Initialize random seed
    srand(time(NULL));

    uint8_t ups_count = 0;
    uint8_t ups_max = 5;
    float   ups_seq_pos[3] = {0.05,0.25,1.0};
    float   ups_seq_neg[3] = {1.0,0.25,0.005};
    float  *ups_seq;

	while (1)
	{
		// Wait timer
		xTaskNotifyWait(0x00, 0xffffffff, NULL, portMAX_DELAY);

		// Get data
		read_telemetry(ina_grid);

        // Get absolute LED intensity
        float led_intensity = entity.led_intensity*LED_INTENSITY_100;

        // Get emergency status
        entity.status = (!gpio_get_level(PIN_SENS_CAR)) | ((!gpio_get_level(PIN_SENS_HELI)) << 0x01) | status_remote;

        switch (entity.status){
            case EMERGENCY_NONE:
            pwm_switch(LED_ROOM_0, LED_INTENSITY_0);
            pwm_switch(LED_ROOM_1, LED_INTENSITY_0);
            break;

            case EMERGENCY_CAR:
            pwm_switch(LED_ROOM_0, led_intensity);
            pwm_switch(LED_ROOM_1, LED_INTENSITY_0);
            break;

            case EMERGENCY_HELI:
            pwm_switch(LED_ROOM_0, LED_INTENSITY_0);
            pwm_switch(LED_ROOM_1, led_intensity);
            break;

            case EMERGENCY_BOTH:
            pwm_switch(LED_ROOM_0, led_intensity);
            pwm_switch(LED_ROOM_1, led_intensity);
            break;
        }

        // Check power source
        entity.power_source = gpio_get_level(PIN_SRC);
        if (entity.power_source == 0)
            ups_seq = ups_seq_neg;
        else
            ups_seq = ups_seq_pos;

        // Show UPS power flow
        ups_count++;
        if (ups_count == ups_max) {
            ups_count = 0;
            
            // Rotate LED intensity sequence
            if (entity.power_source == 0){
                float temp = ups_seq[2];
                ups_seq[2] = ups_seq[1];
                ups_seq[1] = ups_seq[0];
                ups_seq[0] = temp;
            } else {
                float temp = ups_seq[0];
                ups_seq[0] = ups_seq[1];
                ups_seq[1] = ups_seq[2];
                ups_seq[2] = temp;
            }

            for (int ch=LED_UPS_1; ch<LED_MAX; ch++)
                // pwm_fade(ch, ups_seq[ch-LED_UPS_1]*led_intensity,entity_timer_ms*ups_max);
                pwm_switch(ch, ups_seq[ch-LED_UPS_1]*led_intensity);
        }
	}
	vTaskDelete(NULL);
}

//
//------------------------------------------------------------INIT FUNCTIONS-------------------------------------------------------------
//

/**
 * @brief Init GPIO
 * 
 * @return
 *     - ESP_OK Success
 */
esp_err_t init_gpio()
{
    ESP_ERROR_CHECK(gpio_set_direction(PIN_SRC, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_direction(PIN_SENS_HELI, GPIO_MODE_INPUT));
    ESP_ERROR_CHECK(gpio_set_direction(PIN_SENS_CAR,  GPIO_MODE_INPUT));

    return ESP_OK;
}


/**
 * @brief Init PWM channels
 * 
 * @return
 *     - ESP_OK Success
 *     - ESP_ERR_INVALID_STATE Fade function already installed.
 */
esp_err_t init_pwm()
{
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer0 for low speed channels
    // Prepare and set configuration of timer1 for high speed channels
    pwm_timer.speed_mode = PWM_HS_MODE;
    pwm_timer.timer_num = PWM_HS_TIMER;
    ESP_ERROR_CHECK(ledc_timer_config(&pwm_timer));                         // Set configuration of timer1 for high speed channels

    // Set LED Controller with previously prepared configuration
    for (int ch = 0; ch < LED_MAX; ch++)
        ESP_ERROR_CHECK(ledc_channel_config(&pwm_channel[ch]));

    return (ledc_fade_func_install(0));                                     // Initialize fade service
}


/**
 * @brief Init 1-Wire interface
 * 
 *        Initialize the 1-Wire interface and detect the 64-bit ID eeprom connected to
 *        the bus A and B
 * @note  Return ESP_FAIL in case of branch not terminated on a node and trigger a reset
 * @return
 *     - ESP_OK Success
 *     - ESP_FAIL Error
 */
esp_err_t init_owb()
{
    // esp_log_level_set("owb", ESP_LOG_INFO);
    // esp_log_level_set("owb_rmt", ESP_LOG_INFO);

    // Create a 1-Wire bus, using the RMT timeslot driver
    OneWireBus *owb;
    owb_rmt_driver_info rmt_driver_info;
    owb = owb_rmt_initialize(&rmt_driver_info, PIN_EEPROM, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true); // enable CRC check for ROM code
    owb_use_parasitic_power(owb, true);

    // Find connected device
    OneWireBus_SearchState search_state = {0};
    bool found = false;
    owb_search_first(owb, &search_state, &found);
    OneWireBus_ROMCode device_rom_code = search_state.rom_code;
    
    ESP_LOGD(TAG, "Node ID:");
    if (found)
    {
        esp_log_buffer_hex_internal(TAG, device_rom_code.bytes, 8, ESP_LOG_DEBUG);
        memcpy(entity.id, device_rom_code.bytes, 8);
        return ESP_OK;
    }
    else
    {
        ESP_LOGD(TAG, "not valid");
        return ESP_FAIL;
    }
}


/**
 * @brief Init the I2C interface
 * 
 * @return
 *     - ESP_OK   Success
 *     - ESP_ERR_INVALID_ARG Parameter error
 *     - ESP_FAIL Driver install error
 */
esp_err_t init_i2c()
{
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    ESP_LOGD(TAG, "sda_io_num %d", PIN_SDA);

    conf.sda_io_num = PIN_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "scl_io_num %d", PIN_SCL);

    conf.scl_io_num = PIN_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;
    ESP_LOGD(TAG, "clk_speed %d", I2C_MASTER_FREQ_HZ);

    conf.clk_flags = 0;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    ESP_LOGD(TAG, "i2c_param_config %d", conf.mode);
    ESP_ERROR_CHECK(i2c_param_config(i2c_num, &conf));

    ESP_LOGD(TAG, "i2c_driver_install %d", i2c_num);
    return (i2c_driver_install(i2c_num, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0));
}


/**
 * @brief Init the INA233 power monitor
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_telemetry()
{
    /*
    VBus Rail
    | Part      | Count     | Typical | Max     |
    | --------- | :-------: | ------: | ------: |
    | ESP32     | 1         | 160 mA  |  260 mA |
    | LED Lights| 6 		|  30 mA  |  60 mA	|
    | LED Helicopter  | 4	|  20 mA  |  40 mA	|
    | LED Ambulance   | 4	|  20 mA  |  40 mA	|
    | UPS       | 1 		|  		  |  100 mA |
    | INA 223	| 1			|		  |    5 mA	|
    | Total     |           |         |  505 mA |
    */

    float INA_R_SHUNT = 0.100;
    float INA_I_MAX = 0.55;

    ESP_ERROR_CHECK(ina233_init(i2c_num, ina_grid));
    ESP_ERROR_CHECK(ina233_setCalibration(ina_grid, INA_R_SHUNT, INA_I_MAX));
    return ESP_OK;
}


/**
 * @brief Init timer for main entity task
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t init_tasks_timer()
{
	entity_timer_alarm = (uint64_t) entity_timer_ms * TIMER_SCALE / 1000ULL;

	ESP_ERROR_CHECK(timer_init(TIMER_GROUP_0, TIMER_0, &cfg));
	ESP_ERROR_CHECK(timer_pause(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0x00000000ULL));
	ESP_ERROR_CHECK(timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, entity_timer_alarm));
	ESP_ERROR_CHECK(timer_enable_intr(TIMER_GROUP_0, TIMER_0));
	ESP_ERROR_CHECK(timer_isr_register(TIMER_GROUP_0, TIMER_0, entity_timer_isr, NULL, 0, NULL));
    ESP_ERROR_CHECK(timer_start(TIMER_GROUP_0, TIMER_0));
    
	ESP_LOGD(TAG, "Refresh interval set to: %d ms", entity_timer_ms);

	return ESP_OK;
}


/**
 * @brief Init entity
 *        Initialize the entity-specific submodules and start the activity tasks
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t hospital_init_entity()
{
    // Set default LED intensity
    entity.led_intensity = 0.5;

    init_gpio();
    init_pwm();
    init_owb();
    init_i2c();
    init_telemetry();

    // Entity main tasks
	xTaskCreate(entity_main_tasks, "entity_main_task", 2048, NULL, 4, &entity_task_handle);
	ESP_ERROR_CHECK(init_tasks_timer());

    return ESP_OK;
}

//
//------------------------------------------------------------EXTERNAL CALLBACKS-------------------------------------------------------------
//

/**
 * @brief HTTP resp
 *        Callback for responding to HTTP GET requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t hospital_http_resp(char* str)
{
    sprintf(str, "power|%.2f;source|%s",
            entity.power_in.power/1000,
            entity.power_source?"ups":"grid");

	return ESP_OK;
}



/**
 * @brief HTTP read
 *        Callback for reading to HTTP POST requests
 * 
 * @param[in,out] str Message buffer
 * 
 * @return
 *  - ESP_OK : Request correctly processed
 *  - ESP_FAIL : Request fail
 */
esp_err_t hospital_http_read(char *str)
{
    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (msg_unmarshall(str, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
        }

    if (msg_unmarshall(str, "status", &msg_payload)){
            uint8_t val = atoi(msg_payload.val);
            if ((val>=0) && (val<=3)){
                status_remote = val;
                ESP_LOGD(TAG, "STATUS");
            }
            return ESP_OK;
    }

    return ESP_FAIL;
}


/**
 * @brief MQTT init topics
 *        Subscribe to entity-specific topics
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t hospital_mqtt_init(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_service, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_service);

    esp_mqtt_client_subscribe(mqtt_client, mqtt_topic_sub, CONFIG_MQTT_QOS_LEVEL);
    ESP_LOGD(TAG, "Subscribing MQTT topic: %s", mqtt_topic_sub);

    return ESP_OK;
}


/**
 * @brief MQTT post
 *        Callback for periodic publishing updates via MQTT
 * 
 * @param[in] client Pointer to MQTT client handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t hospital_mqtt_post(void *client)
{
    mqtt_client = (esp_mqtt_client_handle_t)client;
    char data[64];
    sprintf(data,"%10s: %7.1f mA [%4.2f V]\tSource: %4s",TAG,entity.power_in.current,entity.power_in.voltage,entity.power_source?"UPS ":"GRID");
    esp_mqtt_client_publish(mqtt_client, mqtt_topic_pub, data, 0, CONFIG_MQTT_QOS_LEVEL, 0);
    ESP_LOGD(TAG, "Outgoing MQTT message: %s\n%s", mqtt_topic_pub, data);
    return ESP_OK;
}


/**
 * @brief MQTT read
 *        Callback for reading subscribed topics via MQTT
 * 
 * @param[in] event Pointer to MQTT event handle
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t hospital_mqtt_read(void *event)
{
    mqtt_event = (esp_mqtt_event_handle_t)event;

    char* data = (char*)malloc(mqtt_event->data_len+1);
    memcpy(data,mqtt_event->data,mqtt_event->data_len);
    data[mqtt_event->data_len] = '\0';

    char* topic = (char*)malloc(mqtt_event->topic_len+1);
    memcpy(topic,mqtt_event->topic,mqtt_event->topic_len);
    topic[mqtt_event->topic_len] = '\0';

    ESP_LOGD(TAG, "Incoming MQTT message: %s\n%s", topic, data);

    // Unmarshal value-data pair
    msg_payload_t msg_payload;

    if (!strcmp(topic,mqtt_topic_service)){
        if (msg_unmarshall(data, "update", &msg_payload))
            if (!strcmp(msg_payload.val,TAG)){
                ESP_LOGD(TAG, "UPDATE");
                // TODO
                return ESP_OK;
            }

        if (msg_unmarshall(data, "light", &msg_payload)){
            float val = atof(msg_payload.val);
            if ((val>=0.0) && (val<=1.0)){
                entity.led_intensity = val;
                ESP_LOGD(TAG, "LIGHT");
            }
            return ESP_OK;
        }
    }
    
    return ESP_OK;
}