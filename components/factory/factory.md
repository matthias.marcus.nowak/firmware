# Factory
The Factory represents a typical industrial consumer with load due to heavy machinery or assembly lines. The power absorbed from the grid is determined by the current working load.

## Main code
### Status structure
```c
struct Factory
{
    uint8_t     id[8];              // Unique port identifier
    float       factory_load;       // Normalized factory load
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> touch[Touch]
    touch --> telemetry[Telemetry]
    telemetry --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> touch[Detect Touch]
    touch --> load[Update Workload]
    load --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/factory**
    - Telemetry
        - *Format*: `Factory: <I> mA [<V> V]   Load: <val> %`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter. Current workload is **val**<br/>(*eg.* `Factory: 121.7 mA [3.36 V]   Load: 75.1 %`)

### Subscribe
- **/legos/factory/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|Factory`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>;load|<val>`
        - *Description*: Log telemetry data, **W** is the input power measured at the meter. Workload **val** is in the range [0.0 to 1.0](*eg.* `power|0.32;load|0.5`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Load
        - Format: `load|<val>`
        - Description: Set workload intensity to **val** [0.0 to 1.0]
### WEB PAGE
[<img src="docs/web_page.png"  width="111" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/factory/factory.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/factory/factory.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/factory/factory.md)