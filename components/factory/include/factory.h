//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _FACTORY_H_
#define _FACTORY_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/touch_pad.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"

#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0  // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1  // Data communication
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_17 // I2C Pins
#define PIN_SCL         GPIO_NUM_5

#define PIN_LOAD_1      GPIO_NUM_25 // Factory Load LED
#define PIN_LOAD_2      GPIO_NUM_26
#define PIN_LOAD_3      GPIO_NUM_27
#define PIN_LOAD_4      GPIO_NUM_14
#define PIN_LOAD_5      GPIO_NUM_12
#define PIN_WELD_W      GPIO_NUM_19 // Factory Welding Blue
#define PIN_WELD_B      GPIO_NUM_21 // Factory Welding White
#define PIN_ROBOT       GPIO_NUM_15 // Factory Robotic Arm

#define IRQ_INA         GPIO_NUM_16 // INA233 Interrupt

// ---------- Touch Definitions ----------
#define PIN_TOUCH_POS   TOUCH_PAD_NUM9     // GPIO_NUM_32
#define PIN_TOUCH_NEG   TOUCH_PAD_NUM8     // GPIO_NUM_33

#define TOUCH_THRESH_NO_USE   (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

// ---------- PWM Channels Definitions ----------
#define PWM_LS_TIMER LEDC_TIMER_0
#define PWM_LS_MODE LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER LEDC_TIMER_1
#define PWM_HS_MODE LEDC_HIGH_SPEED_MODE

enum led_load{
    LED_FACTORY_LOAD_1 = 0,         // GPIO_NUM_25  Factory Load LED
    LED_FACTORY_LOAD_2,             // GPIO_NUM_26
    LED_FACTORY_LOAD_3,             // GPIO_NUM_27
    LED_FACTORY_LOAD_4,             // GPIO_NUM_14
    LED_FACTORY_LOAD_5,             // GPIO_NUM_12
    LED_FACTORY_WELD_W,             // GPIO_NUM_19  Factory Welding Blue
    LED_FACTORY_WELD_B,             // GPIO_NUM_21  Factory Welding White
    PWM_FACTORY_ROBOT,              // GPIO_NUM_15  Factory Robotic Arm
    PWM_FACTORY_MAX,
};

enum servo_pos{
    SERVO_POS_0   = 307,            // Servo position   0°
    SERVO_POS_90  = 614,            // Servo position  90°
    SERVO_POS_180 = 922,            // Servo position 180°
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct Factory
{
    uint8_t     id[8];              // Unique port identifier
    float       factory_load;       // Normalized factory load
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/factory";
static const char mqtt_topic_sub[] = "/legos/factory/cmd";


#ifdef __cplusplus
extern "C" {
#endif

esp_err_t factory_init_entity();
esp_err_t factory_http_resp(char *str);
esp_err_t factory_http_read(char *str);
esp_err_t factory_mqtt_init(void *client);
esp_err_t factory_mqtt_post(void *client);
esp_err_t factory_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

// Callback static linking
#define init_entity() factory_init_entity()
#define http_resp(x) factory_http_resp(x)
#define http_read(x) factory_http_read(x)
#define mqtt_init(x) factory_mqtt_init(x)
#define mqtt_post(x) factory_mqtt_post(x)
#define mqtt_read(x) factory_mqtt_read(x)

#endif //_FACTORY_H_