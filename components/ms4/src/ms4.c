//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#include "ms4.h"
#include "esp_log.h"
#include "string.h"

#include "freertos/FreeRTOS.h"

static const char *TAG = "ms4";

static i2c_port_t i2c_num = I2C_NUM_1;
static uint8_t i2c_address = 0;

static ms4_t *ms4_data;
static uint8_t MS4_SCAN_ALL = 0x7F;


/**
 * @brief  Write to I2C address
 *         Send N bytes from data to register reg
 * 
 * @param[in]  reg   Register address
 * @param[in]  data  Data buffer
 * @param[in]  N     Data size in bytes
 * 
 * @return void
 */
static void i2c_write(uint8_t reg, uint8_t *data, uint8_t N)
{
    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (i2c_address << 1) | I2C_MASTER_WRITE, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_write(cmd, &reg, 1, ACK_CHECK_EN));
    for (i=0; i<N; i++)
        ESP_ERROR_CHECK(i2c_master_write(cmd, &data[i], 1, ACK_CHECK_EN));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    ESP_ERROR_CHECK(i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS));
    i2c_cmd_link_delete(cmd);

    vTaskDelay(1 / portTICK_PERIOD_MS);
}


/**
 * @brief  Read from I2C address
 *         Read N bytes in data from register reg
 * 
 * @param[in]  reg   Register address
 * @param[out] data  Data buffer
 * @param[in]  N     Data size in bytes
 * 
 * @return void
 */
static void i2c_read(uint8_t reg, uint8_t *data, uint8_t size)
{
    // Send register to read from
    i2c_write(reg,NULL,0);

    uint8_t i;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    ESP_ERROR_CHECK(i2c_master_start(cmd));
    ESP_ERROR_CHECK(i2c_master_write_byte(cmd, (i2c_address << 1) | I2C_MASTER_READ, ACK_CHECK_EN));
    for (i = 0; i < size-1; i++)
      ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], ACK_VAL));
    ESP_ERROR_CHECK(i2c_master_read_byte(cmd, &data[i], NACK_VAL));
    ESP_ERROR_CHECK(i2c_master_stop(cmd));
    i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
}


/**
 * @brief  Read all sensors value
 * 
 * @return void
 */
void ms4_readAll()
{
    uint8_t data[16];
    // Get sensor readings
    i2c_read(MS4_REG_STATUS, &data[0], 15);

    ms4_data->status    = data[0];
    ms4_data->temp      = ((double)(data[1]*256  + data[2])) / 10.0;
    ms4_data->hum       = ((double)(data[3]*256  + data[4])) / 10.0;
    ms4_data->light     = ((data[5] << 8) + data[6]);
    ms4_data->batt_v    = ((double)(data[9]*256  + data[10]) / 1024.0) * (3.3 / 0.330);
    ms4_data->co2_ppm   = ((data[11] << 8) + data[12]);
    ms4_data->voc_ppb   = ((data[13] << 8) + data[14]);

    ESP_LOG_BUFFER_HEX_LEVEL(TAG, data, 16, ESP_LOG_DEBUG);

    ESP_LOGD(TAG,"Status:\t%d",ms4_data->status);
    ESP_LOGD(TAG,"Temp:\t%.1f °C",ms4_data->temp);
    ESP_LOGD(TAG,"RH:\t%.1f %%",ms4_data->hum);
    ESP_LOGD(TAG,"Light:\t%d LUX",ms4_data->light);
    ESP_LOGD(TAG,"Batt:\t%.2f V",ms4_data->batt_v);
    ESP_LOGD(TAG,"CO2:\t%d ppm",ms4_data->co2_ppm);
    ESP_LOGD(TAG,"VOC:\t%d ppb\n",ms4_data->voc_ppb);
    
    // Scan all
    i2c_write(MS4_REG_SCAN, &MS4_SCAN_ALL, 1);      
}


/**
 * @brief  Init Ambimate multi-sensor
 * 
 * @return
 *     - ESP_OK   Success
 */
esp_err_t ms4_init(i2c_port_t v_i2c_num, uint8_t addr, ms4_t *data)
{
    i2c_num = v_i2c_num;
    i2c_address = addr;
    ms4_data = data;

    // Enable I2C clock stretching
    ESP_ERROR_CHECK(i2c_set_timeout(i2c_num, 80000));

    uint8_t opt_sensors;
    i2c_read(MS4_REG_OPT, &opt_sensors, 1);
    char opt[10];
    if (opt_sensors & 0x01)
        strcpy(opt, MS4_OPT_CO2);
    else if (opt_sensors & 0x04)
        strcpy(opt, MS4_OPT_AUDIO);
    else
        strcpy(opt, "\0");
    ESP_LOGD(TAG,"AmbiMate sensors: 4 core%s",opt);

    uint8_t fw_ver, fw_sub_ver;
    i2c_read(MS4_REG_FW, &fw_ver, 1);
    i2c_read(MS4_REG_FW, &fw_sub_ver, 1);
    ESP_LOGD(TAG,"AmbiMate version: %d.%d", fw_ver, fw_sub_ver);

    // Scan all
    i2c_write(MS4_REG_SCAN, &MS4_SCAN_ALL, 1);
    vTaskDelay(100 / portTICK_PERIOD_MS);

    return ESP_OK;
}