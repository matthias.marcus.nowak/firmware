# Stadium
The Stadium represents a power-hungry event-based consumer: has a moderate consumption during the training session, but during the a match it becomes significant. The event status can be changed via touch buttons.

## Main code
### Status structure
```c
struct Stadium
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     mode;               // Current operation mode
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
} entity;
```
### Peripherals initialization
```mermaid
flowchart LR
    start([Init]) --> gpio[GPIO]
    gpio --> pwm[PWM]
    pwm --> owb[1-Wire]
    owb --> i2c[I2C]
    i2c --> spi[SPI]
    spi --> touch[Touch]
    touch --> telemetry[Telemetry]
    telemetry --> thread[Spawn tasks thread]
    thread --> stop([Exit])
```
### Periodic tasks
```mermaid
flowchart TD
    init([Init Thread]) --> wait{{wait Timer}}
    wait --> telemetry[Read Telemetry]
    telemetry --> bright[Update Brightness]
    bright --> mode[Update Mode]
    mode --> touch[Detect Touch]
    touch --> sleep[\Sleep/]
    sleep --> wait
    sleep --> delete([Delete Thread])
```

## MQTT callbacks
### Publish
- **/legos/stadium**
    - Telemetry
        - *Format*: `Stadium: <I> mA [<V> V]   Mode: <val>`
        - *Description*: Log telemetry data, **I** and **V** are the current and voltage measured at the meter. Current mode is **val**<br/>(*eg.* `Stadium: 121.7 mA [3.36 V]   Mode: 2`)
    - Mode
        - Format: `stadium|<val>`
        - Description: Log Stadium mode to **val** [0 to 2]

### Subscribe
- **/legos/stadium/cmd**
- **/legos/service/cmd**
    - Update
        - Format: `update|Stadium`
        - Description: Update firmware
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]

## HTTP callbacks
### GET
- **/status**
    - Telemetry
        - *Format*: `power|<W>`
        - *Description*: Log telemetry data, **W** is the input power measured at the meter.<br/>(*eg.* `power|0.32`)
### POST
- **/post**
    - Light
        - Format: `light|<val>`
        - Description: Set light intensity to **val** [0.0 to 1.0]
    - Mode
        - Format: `mode|<val>`
        - Description: Set mode status to **val**: none (0), training (1), match (2)
    - Score
        - Format: `score|<val>`
        - Description: Set score status to **val**: team_b (0), team_a (1)

### WEB PAGE
[<img src="docs/web_page.png"  width="119" height="200">](docs/web_page.png)

## Related Links

* [Concept](https://git.rwth-aachen.de/acs/public/teaching/legos/concept/-/blob/master/application/stadium/stadium.md)
* [Hardware](https://git.rwth-aachen.de/acs/public/teaching/legos/hardware/-/blob/master/entities/stadium/stadium.md) 
* [Assembly](https://git.rwth-aachen.de/acs/public/teaching/legos/assembly/-/blob/master/entities/stadium/stadium.md)