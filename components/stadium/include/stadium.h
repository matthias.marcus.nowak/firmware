//  Copyright:  2021, Institute for Automation of Complex Power Systems, EONERC
//  Contact:    Carlo Guarnieri Calò Carducci <cguarnieri@eonerc.rwth-aachen.de>
//  License:    MIT

#ifndef _STADIUM_H_
#define _STADIUM_H_

#include "esp_system.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "driver/ledc.h"
#include "driver/periph_ctrl.h"
#include "driver/timer.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
#include "driver/i2c.h"
#include "driver/i2s.h"
#include "driver/touch_pad.h"


#include "owb.h"
#include "owb_rmt.h"
#include "ina233.h"
#include "legos_common.h"

// ---------- GPIO Definitions ----------
#define PIN_EEPROM      GPIO_NUM_0      // 1 Wire EEPROM

#define PIN_TXD         GPIO_NUM_1      // Data communication Programming
#define PIN_RXD         GPIO_NUM_3

#define PIN_SDA         GPIO_NUM_13     // I2C Pins
#define PIN_SCL         GPIO_NUM_15

#define PIN_MISO        GPIO_NUM_4      // SPI Pins
#define PIN_MOSI        GPIO_NUM_17
#define PIN_CLK         GPIO_NUM_16
#define PIN_CS		    GPIO_NUM_5
#define PIN_SD  		GPIO_NUM_34     //SD Card detection

#define PIN_TEAM_A      GPIO_NUM_22     // Grandstand & Inside Light 
#define PIN_TEAM_B	    GPIO_NUM_14 
#define PIN_LIGHTS      GPIO_NUM_12 

#define IRQ_INA         GPIO_NUM_18     // INA233 Interrupt Input

// I2S Definitions for Speaker
#define SAMPLE_RATE     (44100)
#define PIN_I2S_BCLK    GPIO_NUM_19
#define PIN_I2S_LRC     GPIO_NUM_21
#define PIN_I2S_DOUT    GPIO_NUM_18

// ---------- Touch Definitions ----------
#define PIN_TOUCH_A     TOUCH_PAD_NUM9     // GPIO_NUM_32
#define PIN_TOUCH_B     TOUCH_PAD_NUM7     // GPIO_NUM_27
#define PIN_TOUCH_M     TOUCH_PAD_NUM8     // GPIO_NUM_33

#define TOUCH_THRESH_NO_USE   (0)
#define TOUCHPAD_FILTER_TOUCH_PERIOD (10)

// ---------- PWM Channel Definitions ----------
#define PWM_LS_TIMER    LEDC_TIMER_0
#define PWM_LS_MODE     LEDC_LOW_SPEED_MODE
#define PWM_HS_TIMER    LEDC_TIMER_1
#define PWM_HS_MODE     LEDC_HIGH_SPEED_MODE

enum led_stadium{
    LED_STADIUM_TEAM_A = 0,
    LED_STADIUM_TEAM_B,
    LED_STADIUM_LIGHTS,
    LED_STADIUM_MAX,
};

enum activity{
    MODE_OFF = 0,
    MODE_TRAINING,
    MODE_MATCH,
    MODE_MAX,
};

// ---------- Entity Definitions ----------

/**
 * @brief Entity status structure
 */
struct Stadium
{
    uint8_t     id[8];              // Unique port identifier
    uint8_t     mode;               // Current operation mode
    float       led_intensity;      // Normalized global brightness
    telemetry_t power_in;           // Input telemetry
} entity;

/**
 * @brief Entity MQTT topics
 */
static const char mqtt_topic_pub[] = "/legos/stadium";
static const char mqtt_topic_sub[] = "/legos/stadium/cmd";


#ifdef __cplusplus
extern "C" {
#endif

// Callback static linking
esp_err_t stadium_init_entity();
esp_err_t stadium_http_resp(char *str);
esp_err_t stadium_http_read(char *str);
esp_err_t stadium_mqtt_init(void *client);
esp_err_t stadium_mqtt_post(void *client);
esp_err_t stadium_mqtt_read(void *event);

#ifdef __cplusplus
}
#endif //__cplusplus

#define init_entity() stadium_init_entity()
#define http_resp(x) stadium_http_resp(x)
#define http_read(x) stadium_http_read(x)
#define mqtt_init(x) stadium_mqtt_init(x)
#define mqtt_post(x) stadium_mqtt_post(x)
#define mqtt_read(x) stadium_mqtt_read(x)

#endif //_STADIUM_H_